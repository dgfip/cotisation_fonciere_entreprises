# README du projet

## Code source du traitement de calcul d'acompte de Cotisation Foncière des entreprises

### Contenu
Les fichiers contenus dans l'archive sont l'ensemble des fichiers de paramétrage utilisés par les services informatiques de la Direction Générale des Finances Publiques pour réaliser le calcul de l'acompte de CFE

Ces fichiers sont développés sous licence CeCILL 2.1 soumise au droit français et respectant les principes de diffusion des logiciels libres.
Ces fichiers sont publiés avec le statut "publié" du guide Etalab d'ouverture des codes sources publics (cf https://github.com/etalab/ouverture-des-codes-sources-publics).
Les contributions extérieures ne seront donc pas traitées.

La vesion est la version 21.1 pour le calcul de l'acompte de CFE 2021.

Les fichiers contenus dans le répertoire src sont notamment :

    Diagramme général des flux :  décrit les flux qui assurent la correspondance entre 
    * les codes issus de l'application et les variables internes au calcul
    * les variables de calcul 

### Quelques éléments de contexte

Le code du calcul de l'acompte de Cotisation foncière est écrit en COBOL.

### Noms d'application
Ces traitements interviennent dans le système d'information en interaction avec d'autres applications, mais surtout utilisent des données relatives aux traitements réalisés antérieurement comme la prise en charge 
de la taxation N-1 ou les différents dégrèvements reçus avant le 31 décembre. 

### Variables
Les variables sont de plusieurs types :

        Les variables saisies par les usagers sur le site impot.fouv (ex. résiliation d'un contrat de mensualisation ou adhésion au prélèvement à l'échéance).
        Les variables calculées ont une formule qui renvoie la valeur de la variable.
        Les variables calculées de base sont des variables qui peuvent être affectées avant le début du calcul.


### Formules

La formule est la suivante :
- base de l'impôt N-1 si supérieure à 3000€ / 2
Elle est décrite dans le pragraphe CALCUL-ACPTE.      
