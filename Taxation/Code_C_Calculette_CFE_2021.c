/*
 * Copyright DGFiP 2021
 * Ce logiciel est régi par la licence CeCILL-2.1 soumise au droit français. 
 * Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les 
 * conditions de la licence CeCILL-2.1 telle que diffusée par le CEA, le CNRS 
 * et l'INRIA sur le site : "http://www.cecill.info/licences/Licence_CeCILL_V2.1-fr.txt"
 */


#include <stdio.h>
#include <math.h>
#include <string.h>


#include "cfe-struc.h"

#define	TRUE	1
#define	FALSE	0



static double arrondi (double donn)
{
	donn=floor(donn + 0.5);
	return donn;
}


static double CalcUneecotis(double ebase,double etaux)
{
	double prod;
	prod=arrondi(ebase*etaux/100000/100);
	return prod;
}


short cfecalc2021 (struct e1 *etaux,struct e2 *ebase,struct e3 *eindic,struct s1 *ecotis, FILE *efrescc)


{
double  NbAnLis  =  5;
double	avfr ;
double  tp;
double	cnp;
double  frais;
double  zttpxc;
double  zztpxc;
double  cbmincne;
double  zttpxs;
double  zztpxs;
double  cbminsy;
double  zttpxe;
double  zztpxe;
double  cbminepci;
double  zttpxt;
double  zztpxt;
double  cbmintse;
double  cexmapi;
double  cexmapicvae;
double  cbminmapi;
double  cextasa;
double  cextasacvae;
double  cbmintasa;
double  zttpxf;
double  zztpxf;
double  fraisbmin;
double  basemini;
double  zonea;
double  zoneaC;
double  zoneaE;
double  zoneavC;
double  zoneavE;
double  zoneb;
double  zonebC;
double  zonebE;
double  zonebvC;
double  zonebvE;
double  zonec;
double  zoned;
double  zonee;
double  zonef;
double  zoneg;
double  zoneh;
double  drfixe;
double  farexo;
double  fdnvexo;
short	trace			=	FALSE;
short	corse			=	FALSE;
short	alsace_moselle	=	FALSE;
short	depdom			=	FALSE;
short	moselle			=	FALSE;
short	grand_lyon   	=	FALSE;
short	st_mart     	=	FALSE;
short   non_auto_entr   =   FALSE;
short   df_seul         =   FALSE;
double	tcm;
double  zttpx;
double  zztpx;
double  F1_cne;
double  F2_cne;
double  F1_epci;
double  F2_epci;
double  DegExoC;
double  DegExoE;
double  DegExocvaeC;
double  DegExocvaeE;

char naf[39][6]=
{
"4939C",
"5030Z",
"5110Z",
"5510Z",
"5520Z",
"5530Z",
"5610A",
"5610B",
"5610C",
"5621Z",
"5629A",
"5629B",
"5630Z",
"5911A",
"5911B",
"5911C",
"5914Z",
"7420Z",
"7721Z",
"7911Z",
"7912Z",
"7990Z",
"8230Z",
"8551Z",
"8552Z",
"9001Z",
"9002Z",
"9003A",
"9004Z",
"9102Z",
"9103Z",
"9104Z",
"9311Z",
"9312Z",
"9313Z",
"9319Z",
"9321Z",
"9329Z",
"9604Z"
};
int i;
short	DegrevementNaf =	FALSE;
const double  LimiteCA = 1.50000000e8;



       avfr=0;
	   tp=0;
	   cnp=0;
	   frais=0;

if (strncmp(eindic->c_csdi,"2A0",sizeof(eindic->c_csdi)) == 0 ||
	strncmp(eindic->c_csdi,"2B0",sizeof(eindic->c_csdi)) == 0 )
{
	corse=TRUE;
}

if (strncmp(eindic->c_csdi,"971",sizeof(eindic->c_csdi)) == 0 &&
	strncmp(eindic->c_ctcn,"127",sizeof(eindic->c_ctcn)) == 0 )
{
	st_mart=TRUE;
}

if (strncmp(eindic->c_csdi,"690",sizeof(eindic->c_csdi)) == 0 &&
	strncmp(eindic->c_ctcngr,"300",sizeof(eindic->c_ctcngr)) == 0 )
{
	grand_lyon=TRUE;
}

trace = (efrescc && eindic &&  1 == eindic->s_zttpzz);

if (strncmp(eindic->c_csdi,"570",sizeof(eindic->c_csdi))==0 ||
	strncmp(eindic->c_csdi,"670",sizeof(eindic->c_csdi))==0 ||
	strncmp(eindic->c_csdi,"680",sizeof(eindic->c_csdi))==0 )
{
	alsace_moselle=TRUE;
}

if (strncmp(eindic->c_csdi,"570",sizeof(eindic->c_csdi))==0 )
{
	moselle=TRUE;
}

if (strncmp(eindic->c_csdi,"971",sizeof(eindic->c_csdi))==0 ||
	strncmp(eindic->c_csdi,"972",sizeof(eindic->c_csdi))==0 ||
	strncmp(eindic->c_csdi,"973",sizeof(eindic->c_csdi))==0 ||
	strncmp(eindic->c_csdi,"974",sizeof(eindic->c_csdi))==0 )
{
	depdom=TRUE;
}

if  ((eindic->s_zteice != 0) || (eindic->d_zteimi == 0 || eindic->c_cieimi != "1"))	
{
	non_auto_entr=TRUE;
}

if (strncmp(eindic->c_cicos,"1",sizeof(eindic->c_cicos))==0)
{
	df_seul=TRUE;
}

for (i=0 ; !DegrevementNaf && i<NbNaf+1 ; i++)
	DegrevementNaf = strncmp(eindic->c_cnac3,naf[i],sizeof(eindic->c_cnac3))==0;
DegrevementNaf = DegrevementNaf && (eindic->d_ztet47 < LimiteCA);


if (trace)
{
	fprintf (efrescc," [- CFECALC2021.C debut -]\n");
	fprintf (efrescc," ************************\n");
	if (grand_lyon)
	{
		fprintf (efrescc,"GRAND LYON\n");
		fprintf (efrescc,"DIRECTION\n");
		fprintf (efrescc,"		c_csdi =%s\n",eindic->c_csdi);
		fprintf (efrescc,"CTCNGR\n");
		fprintf (efrescc,"		c_ctcngr =%s\n",eindic->c_ctcngr);
	}
	else
    {
		fprintf (efrescc,"PAS GRAND LYON\n");
		fprintf (efrescc,"DIRECTION\n");
		fprintf (efrescc,"		c_csdi =%s\n",eindic->c_csdi);
		fprintf (efrescc,"CTCNGR\n");
		fprintf (efrescc,"		c_ctcngr =%s\n",eindic->c_ctcngr);
	}
}




if (df_seul)
{
	if (trace)
	{
		fprintf (efrescc,"********************************\n");
		fprintf (efrescc,"B. IMPOSITION AU DROIT FIXE SEUL\n");
		fprintf (efrescc,"********************************\n");
		fprintf (efrescc," INDICATEUR DROIT FIXE SEUL\n");
		fprintf (efrescc,"		 c_cicos=%s\n",eindic->c_cicos);
		fprintf (efrescc," CODE DIRECTION\n");
		fprintf (efrescc,"		c_csdi=%s\n",eindic->c_csdi);
	}

    if  (non_auto_entr)
	{	
	    ecotis->d_ztchma=etaux->s_ztchdf;
		if (trace)
		{
			fprintf (efrescc,"	a. Indicateurs auto-entrepreneur(s_zteice)(d_zteimi) (c_cieimi) \n");
			fprintf (efrescc,"   ***   \n");
			fprintf (efrescc,"		s_zteice =%d\n",eindic->s_zteice);
			fprintf (efrescc,"		d_zteimi =%.0f\n",eindic->d_zteimi);
			fprintf (efrescc,"		c_cieimi =%s\n",eindic->c_cieimi);
			fprintf (efrescc,"     1*  Non Auto-entrepreneur Montant cotisation avant frais (D_ZTCHMA)\n");
			fprintf (efrescc,"     *   \n");
			fprintf (efrescc,"		d_ztchma =(%d)\n",etaux->s_ztchdf);
			fprintf (efrescc,"		D_ZTCHMA =%.0f\n",ecotis->d_ztchma);
		}
	}

	if (!alsace_moselle)
	{
		ecotis->d_zzchma=arrondi(ecotis->d_ztchma*0.054);
		ecotis->d_zzchmd=arrondi(ecotis->d_ztchma*0.036);
		if (trace)
		{
			fprintf (efrescc,"	FAR TCMA	- Autres departements\n");
			fprintf (efrescc,"	   FAR TCMA =5,4%% de la cotisation avant frais\n");
			fprintf (efrescc,"		D_ZZCHMA= arrondi(%.0f x 5,4%%)\n",ecotis->d_ztchma);
			fprintf (efrescc,"	FDNV TCMA	- Autres departements\n");
			fprintf (efrescc,"	   FDNV TCMA =3,6%% de la cotisation avant frais\n");
			fprintf (efrescc,"		D_ZZCHMD= arrondi(%.0f x 3,6%%)\n",ecotis->d_ztchma);
		}
	}

	if (alsace_moselle)
	{
		ecotis->d_zzchma=arrondi(ecotis->d_ztchma*3/100);
		ecotis->d_zzchmd=arrondi(ecotis->d_ztchma*5/100);
		if (trace)
		{
			fprintf (efrescc,"   FAR TCMA	- Departements d'Alsace-Moselle (570,670 et 680)\n");
			fprintf (efrescc,"		*FAR TCMA = 3%% de la cotisation avant frais\n");
			fprintf (efrescc,"		D_ZZCHMA= arrondi(%.0f x 3%%)\n",ecotis->d_ztchma);
			fprintf (efrescc,"   FDNV TCMA	- Departements d'Alsace-Moselle (570,670 et 680)\n");
			fprintf (efrescc,"		*FDNV TCMA = 5%% de la cotisation avant frais\n");
			fprintf (efrescc,"		D_ZZCHMD= arrondi(%.0f x 5%%)\n",ecotis->d_ztchma);
		}
	}
	
	ecotis->d_ztchm=ecotis->d_ztchma+ecotis->d_zzchma+ecotis->d_zzchmd;
	ecotis->d_zttpct=ecotis->d_ztchm;
	ecotis->d_zttpcp=ecotis->d_ztchm;
	if (trace)
	{
		fprintf (efrescc,"	c. Montant cotisation apres frais (D_ZTCHM)\n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"		*cotisation TCM apres frais=somme de la cotisation avant frais et des frais de\n");
		fprintf (efrescc,"     		gestion\n");
		fprintf (efrescc,"		 D_ZTCHM= %.0f + %.0f + %.0f \n",ecotis->d_ztchma,ecotis->d_zzchma,ecotis->d_zzchmd);
		fprintf (efrescc,"		 D_ZTCHM=%.0f\n",ecotis->d_ztchm);
		fprintf (efrescc,"		*cotisation mise en recouvrement=Somme a payer=cotisation apres frais de\n");
		fprintf (efrescc,"     	gestion\n");
		fprintf (efrescc,"		 D_ZTTPCT=%.0f\n",ecotis->d_zttpct);
		fprintf (efrescc,"		Somme a payer D_ZTTPCP=%.0f\n",ecotis->d_zttpcp);
		fprintf (efrescc,"	    FIN    					\n");
	}
return 0;
	
}

if (trace)
{
	fprintf (efrescc,"C. IMPOSITION << NORMALE >>\n");
	fprintf (efrescc,"****************************\n");
	fprintf (efrescc,"   ***   \n");
}

if (!st_mart)
{
	basemini = 0;
	if (corse)
	{
		basemini=arrondi(etaux->d_ztbsbm * 0.75);
		if (trace)
		{
			fprintf (efrescc,"		a) Departements de Corse (2A0 et 2B0)\n");
			fprintf (efrescc,"		*Base de cotisation minimale=75%% de la base \n");
			fprintf (efrescc,"		d_ztbsbm=arrondi(%.0f x 75%%)\n",etaux->d_ztbsbm);
		}
	}
	else
	{
		basemini = etaux->d_ztbsbm;
		if (trace)
		{
			fprintf (efrescc,"		b) Autres departements \n");
			fprintf (efrescc,"		*Base de cotisation minimale=84%% de la base apres base mini\n");
			fprintf (efrescc,"		d_ztbsbm=%.0f \n",etaux->d_ztbsbm);
		}
	}


	zttpxc = 0;
	zztpxc = 0;
	cbmincne = 0;
	ecotis->d_ztcnct=CalcUneecotis(ebase->d_ztbsco,etaux->d_ztcntp);
	zttpxc=CalcUneecotis(ebase->d_zzcnex,etaux->d_ztcntp);
	zztpxc=CalcUneecotis(ebase->d_zxcnex,etaux->d_ztcntp);
	cbmincne=CalcUneecotis(basemini,etaux->d_ztcntp);
	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	1. cotisations commune(d_ztcnct),cotisation exoneree commune(zttpxc)\n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	* cotisation commune=produit de la ebase taxable commune par le etaux de\n");
		fprintf (efrescc,"     	taxation commune divise par 10 000 000\n");
		fprintf (efrescc,"	 d_ztcnct=CalcUneecotis(%.0f,%.0f)\n",ebase->d_ztbsco,etaux->d_ztcntp);
		fprintf (efrescc,"	 D_ZTCNCT=%.0f\n",ecotis->d_ztcnct);
		fprintf (efrescc,"	* cotisation exoneree commune =produit de la ebase exoneree commune par le etaux de\n");
		fprintf (efrescc,"     	taxation commune divise par 10 000 000\n");
		fprintf (efrescc,"	 zttpxc=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zzcnex,etaux->d_ztcntp);
		fprintf (efrescc,"	 zttpxc=%.0f\n",zttpxc);
		fprintf (efrescc,"	* cotisation exoneree commune CVAE=produit de la ebase exoneree commune CVAE par le etaux \n");
		fprintf (efrescc,"     	de taxation commune divise par 10 000 000\n");
		fprintf (efrescc,"	1. cotisations  exoneree commune CVAE(zztpxc)\n");
		fprintf (efrescc,"	 zztpxc=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zxcnex,etaux->d_ztcntp);
		fprintf (efrescc,"	 zztpxc=%.0f\n",zztpxc);
		fprintf (efrescc,"	* cotisation sur ebase minimum cne =produit de la ebase minimum commune par le etaux de\n");
		fprintf (efrescc,"     	taxation commune divise par 10 000 000\n");
		fprintf (efrescc,"	1. cotisations sur ebase minimum commune(cbmincne)\n");
		fprintf (efrescc,"	 cbmincne=CalcUneecotis(%.0f,%.0f)\n",basemini,etaux->d_ztcntp);
		fprintf (efrescc,"	 CBMINCNE=%.0f\n",cbmincne);
	}

	zttpxs = 0;
	zztpxs = 0;
	cbminsy = 0;
	if (etaux->d_ztcnsy > 0)
	{
		ecotis->d_ztcncs=CalcUneecotis(ebase->d_ztbsco,etaux->d_ztcnsy);
		zttpxs=CalcUneecotis(ebase->d_zzcnex,etaux->d_ztcnsy);
		zztpxs=CalcUneecotis(ebase->d_zxcnex,etaux->d_ztcnsy);
		cbminsy=CalcUneecotis(basemini,etaux->d_ztcnsy);
		if (trace)
		{
			fprintf (efrescc,"   ***   \n");
			fprintf (efrescc,"	1.2 cotisations syndicat(d_ztcncs),cotisation exoneree syndicat(zttpxs)\n");
			fprintf (efrescc,"   ***   \n");
			fprintf (efrescc,"	* cotisation syndicat=produit de la ebase taxable syndicale par le etaux de\n");
			fprintf (efrescc,"     	taxation syndicat divise par 10 000 000\n");
			fprintf (efrescc,"	 d_ztcncs=CalcUneecotis(%.0f,%.0f)\n",ebase->d_ztbsco,etaux->d_ztcnsy);
			fprintf (efrescc,"	 D_ZTCNCS=%.0f\n",ecotis->d_ztcncs);
			fprintf (efrescc,"	* cotisation exoneree syndicat =produit de la ebase exoneree commune par le etaux de\n");
			fprintf (efrescc,"     	taxation syndicat divise par 10 000 000\n");
			fprintf (efrescc,"	 zttpxs=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zzcnex,etaux->d_ztcnsy);
			fprintf (efrescc,"	 zttpxs=%.0f\n",zttpxs);
			fprintf (efrescc,"	* cotisation exoneree syndicat CVAE=produit de la ebase exoneree commune CVAE par le\n");
			fprintf (efrescc,"     	etaux de taxation syndicat divise par 10 000 000\n");
			fprintf (efrescc,"	1. cotisations  exoneree syndicat CVAE(zztpxs)\n");
			fprintf (efrescc,"	 zztpxs=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zxcnex,etaux->d_ztcnsy);
			fprintf (efrescc,"	 zztpxs=%.0f\n",zztpxs);
			fprintf (efrescc,"	*1.3.cotisation sur ebase minimum syndicat =produit de la ebase minimum syndicat par le etaux de\n");
			fprintf (efrescc,"     	taxation syndicat divise par 10 000 000\n");
			fprintf (efrescc,"	 cbminsy=CalcUneecotis(%.0f,%.0f)\n",basemini,etaux->d_ztcnsy);
			fprintf (efrescc,"	 CBMINSY=%.0f\n",cbminsy);
		}
	}


	if (trace)
	{
		fprintf (efrescc,"          ***   \n");
	    fprintf (efrescc,"	    	*indicateur code TSE(s_ztcnep) \n");
		fprintf (efrescc,"		 s_ztcnep=%d\n",eindic->s_ztcnep);
		fprintf (efrescc,"******************************************** \n");
	}
	ecotis->d_ztcnce = 0;
	zttpxt = 0;
	zztpxt= 0;
	ecotis->d_zttpgp = 0;
	zttpxt = 0;
	zztpxt = 0;
	cbmintse = 0;
	if (eindic->s_ztcnep == 1)
	{
		if (trace)
		{
			fprintf (efrescc,"   ***   \n");
			fprintf (efrescc,"	2. TAUX TSE (D_ZTCH,)\n");
			fprintf (efrescc,"   ***   \n");
			fprintf (efrescc,"		*Taux TSE=Taux TSE - Taux TASARIF\n");
			fprintf (efrescc,"		 d_ztch=%.0f-%.0f)\n",etaux->d_ztch,etaux->d_ztcht3);
		}
		etaux->d_ztch=etaux->d_ztch-etaux->d_ztcht3;
		ecotis->d_ztcnce=CalcUneecotis(ebase->d_ztbsts,etaux->d_ztch);
		if (trace)
		{
  			fprintf (efrescc,"		 D_ZTCH=%.0f\n",etaux->d_ztch);
			fprintf (efrescc,"   ***   \n");
			fprintf (efrescc,"	2. cotisations TSE (D_ZTCNCE,)\n");
			fprintf (efrescc,"   ***   \n");
			fprintf (efrescc,"		* cotisation TSE=produit de la ebase TSE par le etaux de taxation TSE\n");
			fprintf (efrescc,"     		divise par 10 000 000\n");
			fprintf (efrescc,"		 d_ztcnce=CalcUneecotis(%.0f,%.0f)\n",ebase->d_ztbsts,etaux->d_ztch);
			fprintf (efrescc,"		 D_ZTCNCE=%.0f\n",ecotis->d_ztcnce);
		}

		if (trace)
		{
			fprintf (efrescc,"  ***   \n");
			fprintf (efrescc,"	 cotisations exoneree TSE (zttpxt) \n");
			fprintf (efrescc,"		*** INDICATEURS***\n");
			fprintf (efrescc,"		*****************\n");
			fprintf (efrescc,"		 C_CICUU=%s\n",etaux->c_cicuu);
			fprintf (efrescc,"		***\n");
			fprintf (efrescc,"		*** ZONE        ***\n");	
			fprintf (efrescc,"		 C_CIETZ=%s\n",eindic->c_cietz);
			fprintf (efrescc,"		*****************\n");
		}
		if ((strncmp(etaux->c_cicuu,"U",sizeof(etaux->c_cicuu)) == 0) || (strncmp(eindic->c_cietz, "Z",sizeof(eindic->c_cietz)) == 0) || (strncmp(eindic->c_cietz, "O",sizeof(eindic->c_cietz)) == 0))
		{
			zttpxt=CalcUneecotis(ebase->d_zzcuex,etaux->d_ztch);
			zztpxt=CalcUneecotis(ebase->d_zxcuex,etaux->d_ztch);
			if (trace)
			{
				fprintf (efrescc,"	 *cotisation exoneree tse =produit de la ebase exoneree epci par le etaux de\n");
				fprintf (efrescc,"     	taxation tse divise par 10 000 000\n");
				fprintf (efrescc,"	 zttpxt=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zzcuex,etaux->d_ztch);
				fprintf (efrescc,"	 zttpxt=%.0f\n",zttpxt);
				fprintf (efrescc,"	 *cotisation exoneree tse cvae =produit de la ebase exoneree epci cvae par\n");
				fprintf (efrescc,"     	le etaux de taxation tse divise par 10 000 000\n");
				fprintf (efrescc,"	 zztpxt=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zxcuex,etaux->d_ztch);
				fprintf (efrescc,"	 zztpxt=%.0f\n",zztpxt);
			}
		}
		else
		{
		    zttpxt=CalcUneecotis(ebase->d_zzcnex,etaux->d_ztch);
			zztpxt=CalcUneecotis(ebase->d_zxcnex,etaux->d_ztch);
			if (trace)
			{
				fprintf (efrescc,"	 *cotisation exoneree tse =produit de la ebase exoneree epci par le etaux de\n");
				fprintf (efrescc,"     	taxation tse divise par 10 000 000\n");
				fprintf (efrescc,"	 zttpxt=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zzcnex,etaux->d_ztch);
				fprintf (efrescc,"	 zttpxt=%.0f\n",zttpxt);
				fprintf (efrescc,"	 *cotisation exoneree tse cvae =produit de la ebase exoneree epci cvae par\n");
				fprintf (efrescc,"     	le etaux de taxation tse divise par 10 000 000\n");
				fprintf (efrescc,"	 zztpxt=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zxcnex,etaux->d_ztch);
				fprintf (efrescc,"	 zztpxt=%.0f\n",zztpxt);
			}
		}

		cbmintse=CalcUneecotis(basemini,etaux->d_ztch);
		if (trace)
		{
			fprintf (efrescc,"	*cotisation sur ebase minimum TSE =produit de la ebase minimum TSE par le etaux de\n");
			fprintf (efrescc,"     	taxation tse divise par 10 000 000\n");
			fprintf (efrescc,"	Cotisations sur ebase minimum TSE(cbmintse)\n");
			fprintf (efrescc,"	 cbmintse=CalcUneecotis(%.0f,%.0f)\n",basemini,etaux->d_ztch);
			fprintf (efrescc,"	 CBMINTSE=%.0f\n",cbmintse);
		}

		ecotis->d_zttpgp=CalcUneecotis(ebase->d_ztbsts,etaux->d_ztchgp);
		if (trace)
		{
			fprintf (efrescc,"	Cotisations  TSE Grand Paris (D_ZTTPGP)\n");
			fprintf (efrescc,"	 d_zttpgp=CalcUneecotis(%.0f,%.0f)\n",ebase->d_ztbsts,etaux->d_ztchgp);
			fprintf (efrescc,"	 D_ZTTPGP=%.0f\n",ecotis->d_zttpgp);
		}
	}

	cextasa = 0;
	cextasacvae = 0;
	cbmintasa= 0;
	cexmapi = 0;
	cexmapicvae = 0;
	cbminmapi = 0;
	if (trace)
	{
		fprintf (efrescc,"          ***   \n");
	    fprintf (efrescc,"	    	*indicateur code TSE(s_ztcnep) \n");
		fprintf (efrescc,"		 s_ztcnep=%d\n",eindic->s_ztcnep);
		fprintf (efrescc,"******************************************** \n");
	}
	if  (eindic->s_ztcnep == 1)
	{
		ecotis->d_ztrgif=CalcUneecotis(ebase->d_ztbsif,etaux->d_ztcht3);
		if (trace)
		{
			fprintf (efrescc,"	 *cotisation TASARIF=produit de la ebase tasarif par le etaux\n");
			fprintf (efrescc,"     	de taxation TASARIF divise par 10 000 000\n");
			fprintf (efrescc,"	 ecotis->d_ztrgif=CalcUneecotis(%.0f,%.0f)\n",ebase->d_ztbsif,etaux->d_ztcht3);
			fprintf (efrescc,"	 D_ZTRGIF=%.0f\n",ecotis->d_ztrgif);
		}

		if ((strncmp(etaux->c_cicuu,"U",sizeof(etaux->c_cicuu)) == 0) || (strncmp(eindic->c_cietz, "Z",sizeof(eindic->c_cietz)) == 0) || (strncmp(eindic->c_cietz, "O",sizeof(eindic->c_cietz)) == 0))
		{
			cextasa=CalcUneecotis(ebase->d_zzcuex,etaux->d_ztcht3);
			cextasacvae=CalcUneecotis(ebase->d_zxcuex,etaux->d_ztcht3);
			if (trace)
			{
				fprintf (efrescc,"	 *cotisation exoneree TASARIF =produit de la ebase TASARIF par le etaux de\n");
				fprintf (efrescc,"     	taxation TASARIF divise par 10 000 000\n");
				fprintf (efrescc,"	 cextasa=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zzcuex,etaux->d_ztcht3);
				fprintf (efrescc,"	 CEXTASA=%.0f\n",cextasa);
				fprintf (efrescc,"	 *cotisation exoneree TASARIF CVAE=produit de la ebase exoneree TASARIF cvae par\n");
				fprintf (efrescc,"     	le etaux de taxation TASARIF divise par 10 000 000\n");
				fprintf (efrescc,"	 cextasacvae=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zxcuex,etaux->d_ztcht3);
				fprintf (efrescc,"	 CEXTASACVAE=%.0f\n",cextasacvae);
			}
		}
		else
		{
			cextasa=CalcUneecotis(ebase->d_zzcnex,etaux->d_ztcht3);
			cextasacvae=CalcUneecotis(ebase->d_zxcnex,etaux->d_ztcht3);
			if (trace)
			{
				fprintf (efrescc,"	 *cotisation exoneree TASARIF =produit de la ebase exoneree commune par le etaux de\n");
				fprintf (efrescc,"     	taxation TASARIF divise par 10 000 000\n");
				fprintf (efrescc,"	 cextasa=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zzcnex,etaux->d_ztcht3);
				fprintf (efrescc,"	 CEXTASA=%.0f\n",cextasa);
				fprintf (efrescc,"	 *cotisation exoneree TASARIF cvae=produit de la ebase exoneree commune cvae par le etaux\n");
				fprintf (efrescc,"     	de taxation TASARIF divise par 10 000 000\n");
				fprintf (efrescc,"	 cextasacvae=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zxcnex,etaux->d_ztcht3);
				fprintf (efrescc,"	 CEXTASACVAE=%.0f\n",cextasacvae);
			}
		}

		cbmintasa=CalcUneecotis(basemini,etaux->d_ztcht3);
		if (trace)
		{
			fprintf (efrescc,"	*cotisation sur ebase minimum TASARIF =produit de la ebase minimum TASARIF par le etaux de\n");
			fprintf (efrescc,"     	taxation TASARIF divise par 10 000 000\n");
			fprintf (efrescc,"	Cotisations sur ebase minimum TASARIF(cbmintasa)\n");
			fprintf (efrescc,"	 cbmintasa=CalcUneecotis(%.0f,%.0f)\n",basemini,etaux->d_ztcht3);
			fprintf (efrescc,"	 CBMINTASA=%.0f\n",cbmintasa);
		}
	}	

	if (trace)
	{
		fprintf (efrescc,"  ** INDICATEUR Collectivite TGEMAPI **\n");					
        fprintf (efrescc,"      C_CITPAI =%s\n",eindic->c_citpai);
	}

	if ((strncmp(eindic->c_citpai, "E",sizeof(eindic->c_citpai)) == 0) || (strncmp(eindic->c_citpai, "C",sizeof(eindic->c_citpai)) == 0))
	{
		ecotis->d_ztcuge=CalcUneecotis(ebase->d_ztbsge,etaux->d_ztcht4);
		if (trace)
		{
			fprintf (efrescc,"	 *cotisation TGEMAPI=produit de la ebase tgemapi par le etaux\n");
			fprintf (efrescc,"     	de taxation TGMAPI divise par 10 000 000\n");
			fprintf (efrescc,"	 ecotis->d_ztcuge=CalcUneecotis(%.0f,%.0f)\n",ebase->d_ztbsge,etaux->d_ztcht4);
			fprintf (efrescc,"	 D_ZTCUGE=%.0f\n",ecotis->d_ztcuge);
		}
		if ((strncmp(etaux->c_cicuu,"U",sizeof(etaux->c_cicuu)) == 0) || (strncmp(eindic->c_cietz, "Z",sizeof(eindic->c_cietz)) == 0) || (strncmp(eindic->c_cietz, "O",sizeof(eindic->c_cietz)) == 0))
		{
			cexmapi=CalcUneecotis(ebase->d_zzcuex,etaux->d_ztcht4);
			cexmapicvae=CalcUneecotis(ebase->d_zxcuex,etaux->d_ztcht4);
			if (trace)
			{
				fprintf (efrescc,"	 *cotisation exoneree TGEMAPI =produit de la ebase TGEMAPI par le etaux de\n");
				fprintf (efrescc,"     	taxation TGEMAPI divise par 10 000 000\n");
				fprintf (efrescc,"	 cexmapi=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zzcuex,etaux->d_ztcht4);
				fprintf (efrescc,"	 CEXMAPI=%.0f\n",cexmapi);
				fprintf (efrescc,"	 *cotisation exoneree TGEMAPI CVAE=produit de la ebase exoneree TGEMAPI cvae par\n");
				fprintf (efrescc,"     	le etaux de taxation TGEMAPI divise par 10 000 000\n");
				fprintf (efrescc,"	 cexmapicvae=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zxcuex,etaux->d_ztcht4);
				fprintf (efrescc,"	 CEXMAPICVAE=%.0f\n",cexmapicvae);
			}
		}
		else
		{
			cexmapi=CalcUneecotis(ebase->d_zzcnex,etaux->d_ztcht4);
			cexmapicvae=CalcUneecotis(ebase->d_zxcnex,etaux->d_ztcht4);
			if (trace)
			{
				fprintf (efrescc,"	 *cotisation exoneree TGEMAPI =produit de la ebase exoneree commune par le etaux de\n");
				fprintf (efrescc,"     	taxation TGEMAPI divise par 10 000 000\n");
				fprintf (efrescc,"	 cexmapi=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zzcnex,etaux->d_ztcht4);
				fprintf (efrescc,"	 CEXMAPI=%.0f\n",cexmapi);
				fprintf (efrescc,"	 *cotisation exoneree TGEMAPI cvae=produit de la ebase exoneree commune cvae par le etaux\n");
				fprintf (efrescc,"     	de taxation TGEMAPI divise par 10 000 000\n");
				fprintf (efrescc,"	 cexmapicvae=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zxcnex,etaux->d_ztcht4);
				fprintf (efrescc,"	 CEXMAPICVAE=%.0f\n",cexmapicvae);
			}
		}

		cbminmapi=CalcUneecotis(basemini,etaux->d_ztcht4);
		if (trace)
		{
			fprintf (efrescc,"	*cotisation sur ebase minimum TGEMAPI =produit de la ebase minimum TGEMAPI par le etaux de\n");
			fprintf (efrescc,"     	taxation TGEMAPI divise par 10 000 000\n");
			fprintf (efrescc,"	Cotisations sur ebase minimum TGEMAPI(cbminmapi)\n");
			fprintf (efrescc,"	 cbminmapi=CalcUneecotis(%.0f,%.0f)\n",basemini,etaux->d_ztcht4);
			fprintf (efrescc,"	 CBMINMAPI=%.0f\n",cbminmapi);
		}
	}	

	zttpxe = 0;
	zztpxe = 0;
	cbminepci = 0;

	ecotis->d_ztcncg=CalcUneecotis(ebase->d_ztbsgp,etaux->d_ztcngp);
	zttpxe=CalcUneecotis(ebase->d_zzcuex,etaux->d_ztcngp);
	zztpxe=CalcUneecotis(ebase->d_zxcuex,etaux->d_ztcngp);
	cbminepci=CalcUneecotis(basemini,etaux->d_ztcngp);

	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	3. cotisations EPCI (D_ZTCNCG)\n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"		*cotisation EPCI=produit de la ebase taxable EPCI par le etaux de taxation\n");
		fprintf (efrescc,"     	EPCI divise par 10 000 000\n");
		fprintf (efrescc,"		 d_ztcncg=CalcUneecotis(%.0f,%.0f)\n",ebase->d_ztbsgp,etaux->d_ztcngp);
		fprintf (efrescc,"		 D_ZTCNCG=%.2f\n",ecotis->d_ztcncg);
		fprintf (efrescc,"	*cotisation exoneree EPCI=produit de la ebase exoneree EPCI par le etaux de\n");
		fprintf (efrescc,"     	taxation EPCI divise par 10 000 000\n");
		fprintf (efrescc,"	 zttpxe=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zzcuex,etaux->d_ztcngp);
		fprintf (efrescc,"	 zttpxe=%.0f\n",zttpxe);
		fprintf (efrescc,"	*cotisation exoneree EPCI CVAE=produit de la base exoneree EPCI CVAE par le etaux de \n");
		fprintf (efrescc,"     	taxation EPCI divise par 10 000 000\n");
		fprintf (efrescc,"	1. cotisations  exoneree EPCI CVAE(zztpxe)\n");
		fprintf (efrescc,"	 zztpxe=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zxcuex,etaux->d_ztcngp);
		fprintf (efrescc,"	 zztpxe=%.0f\n",zztpxe);
		fprintf (efrescc,"	*cotisation sur ebase minimum EPCI =produit de la ebase minimum EPCI par le etaux de\n");
		fprintf (efrescc,"     	taxation EPCI divise par 10 000 000\n");
		fprintf (efrescc,"	1. cotisations sur ebase minimum EPCI(cbminepci)\n");
		fprintf (efrescc,"	 cbminepci=CalcUneecotis(%.0f,%.0f)\n",basemini,etaux->d_ztcngp);
		fprintf (efrescc,"	 CBMINEPCI=%.0f\n",cbminepci);
	}

	ecotis->d_zttpav=ecotis->d_ztcnct +			
				ecotis->d_ztcncs +
				ecotis->d_ztcncg +
				ecotis->d_ztcnce +
				ecotis->d_ztcuge +
				ecotis->d_ztrgif;
	ecotis->d_zttpx=zttpxc+zttpxs+zttpxt+cexmapi+cextasa+zttpxe;
	ecotis->d_zztpx=zztpxc+zztpxs+zztpxt+cexmapicvae+cextasacvae+zztpxe;
				
	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	5. cotisations CFE avant frais de gestion (D_ZTTPAV\n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"		*cotisation CFE avant frais=somme des cotisations \n");
		fprintf (efrescc,"     	commune, syndicat,  TSE et EPCI\n");
		fprintf (efrescc," d_zttpav=%.0f + %.0f + %.0f + %.0f + %.0f \n",ecotis->d_ztcnct,ecotis->d_ztcncs,ecotis->d_ztcncg,ecotis->d_ztcnce,ecotis->d_ztcuge,ecotis->d_ztrgif);
		fprintf (efrescc," D_ZTTPAV=%.0f\n",ecotis->d_zttpav);
		fprintf (efrescc,"		*cotisation exoneree CFE avant frais=somme des cotisations exo\n");
		fprintf (efrescc,"     	commune, syndicat,  TSE et EPCI\n");
		fprintf (efrescc," d_zttpx=%.0f+%.0f + %.0f + %.0f + %.0f + %.0f \n",zttpxc,zttpxs,zttpxt,cexmapi,cextasa,zttpxe);
		fprintf (efrescc," D_ZTTPX=%.0f\n",ecotis->d_zttpx);
		fprintf (efrescc,"		*cotisation exo CVAE avant frais=somme des cotisations exo CVAE\n");
		fprintf (efrescc,"     	commune, syndicat,  TSE et EPCI\n");
		fprintf (efrescc," d_zztpx=%.0f+%.0f + %.0f + %.0f + %.0f + %.0f \n",zztpxc,zztpxs,zztpxt,cexmapicvae,cextasacvae,zztpxe);
		fprintf (efrescc," D_ZZTPX=%.0f\n",ecotis->d_zztpx);
	}
				

	ecotis->d_ztchci=CalcUneecotis(ebase->d_ztbsci,etaux->d_ztcnci);
	ecotis->d_zttpxi=CalcUneecotis(ebase->d_zzchxi,etaux->d_ztcnci);

	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	6. COTISATION TCCI BRUTE AVANT FRAIS DE GESTION (D_ZTCHCI)\n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"		*COTISATION TCCI=produit de la ebase taxable TCCI par le etaux de taxation\n");
		fprintf (efrescc,"     	TCCI divise par 10 000 00\n");
		fprintf (efrescc,"		*COTISATION TCCI=produit de la ebase taxable TCCI par le etaux de taxation\n");
		fprintf (efrescc,"     	TCCI divise par 10 000 000\n");
		fprintf (efrescc,"		 d_ztchci=CalcUneecotis(%.0f,%.0f)\n",ebase->d_ztbsci,etaux->d_ztcnci);
		fprintf (efrescc,"		 D_ZTCHCI=%.0f\n",ecotis->d_ztchci);
		fprintf (efrescc,"		*COTISATION TCCI EXONEREE=produit de la ebase taxable TCCI par le etaux de taxation\n");
		fprintf (efrescc,"     	TCCI divise par 10 000 000\n");
		fprintf (efrescc,"		 d_zttpxi=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zzchxi,etaux->d_ztcnci);
		fprintf (efrescc,"		 D_ZTTPXI=%.0f\n",ecotis->d_zttpxi);
	}


	drfixe=0;
	if (!non_auto_entr)
	{
		ecotis->d_ztchma=0;
		ecotis->d_ztchcm=0;
		if (trace)
		{
			fprintf (efrescc,"     a)*  Auto-entrepreneur \n");
			fprintf (efrescc,"     *   \n");
			fprintf (efrescc,"		D_ZTCHMA =%.0f\n",ecotis->d_ztchma);
			fprintf (efrescc,"		D_ZTCHCM =%.0f\n",ecotis->d_ztchcm);
		}
	}
	else
	{
		if (trace)
		{
			fprintf (efrescc,"          ***   \n");
			fprintf (efrescc,"		b) Toutes communes \n");
			fprintf (efrescc,"          ***   \n");
			fprintf (efrescc,"	    	*indicateur droit additionnel(s_ztcoda) \n");
			fprintf (efrescc,"		 s_ztcoda=%d\n",eindic->s_ztcoda);
			fprintf (efrescc,"******************************************** \n");
		}
		if (eindic->s_ztcoda==1)
		{	
			ecotis->d_ztchcm=CalcUneecotis(ebase->d_ztbscm,etaux->d_ztch4);
			ecotis->d_zttpxm=CalcUneecotis(ebase->d_zzchxm,etaux->d_ztch4);
			if (trace)
			{
				fprintf (efrescc,"		 Droit additionnel (D_ZTCHCM)\n");
				fprintf (efrescc,"		* cotisation droit additionnel=produit de la ebase TCM par le etaux de\n");
				fprintf (efrescc,"     	taxation TCM divise par 10 000 000\n");
				fprintf (efrescc,"		 d_ztchcm=CalcUneecotis(%.0f,%.0f)\n",ebase->d_ztbscm,etaux->d_ztch4);
				fprintf (efrescc,"		 D_ZTCHCM=%.0f\n",ecotis->d_ztchcm);
				fprintf (efrescc,"		 Droit additionnel exonere (D_ZTTPXM)\n");
				fprintf (efrescc,"		* cotisation droit additionnel exonere=produit de la ebase TCM par le etaux de\n");
				fprintf (efrescc,"     	taxation TCM divise par 10 000 000\n");
				fprintf (efrescc,"		 d_zttpxm=CalcUneecotis(%.0f,%.0f)\n",ebase->d_zzchxm,etaux->d_ztch4);
				fprintf (efrescc,"		 D_ZTTPXM=%.0f\n",ecotis->d_zttpxm);
			}
		}
		if (trace)
		{
			fprintf (efrescc,"		 Droit fixe \n");
			fprintf (efrescc,"		*indicateur droit fixe(s_ztcodf) \n");
			fprintf (efrescc,"		 s_ztcodf=%d\n",eindic->s_ztcodf);
			fprintf (efrescc,"******************************************** \n");
		}
		if (eindic->s_ztcodf==1)
		{
			if (trace)
			{
				fprintf (efrescc,"	a. Indicateurs auto-entrepreneur(s_zteice)(d_zteimi) (c_cieimi) \n");
				fprintf (efrescc,"   ***   \n");
				fprintf (efrescc,"		s_zteice =%d\n",eindic->s_zteice);
				fprintf (efrescc,"		d_zteimi =%.0f\n",eindic->d_zteimi);
				fprintf (efrescc,"		c_cieimi =%s\n",eindic->c_cieimi); 	
			}
			drfixe=etaux->s_ztchdf;
			if (trace)
			{	
				fprintf (efrescc,"     Montant droit fixe \n");
				fprintf (efrescc,"     *   \n");
				fprintf (efrescc,"		droit fixe =(%.d)\n",etaux->s_ztchdf);
				fprintf (efrescc,"		DRFIXE =%.0f\n",drfixe);
			}
		}
	}



		ecotis->d_ztcnlt=ecotis->d_ztcnct-NbAnLis*ebase->d_zzcnct;
		if (ecotis->d_ztcnlt<0) ecotis->d_ztcnlt=0;
		ecotis->d_ztcnls=ecotis->d_ztcncs-NbAnLis*ebase->d_zzcncs;
		if (ecotis->d_ztcnls<0) ecotis->d_ztcnls=0;
		ecotis->d_ztcnlg=ecotis->d_ztcncg-NbAnLis*ebase->d_zzcncg;
		if (ecotis->d_ztcnlg<0) ecotis->d_ztcnlg=0;
		ecotis->d_ztcnle=ecotis->d_ztcnce-NbAnLis*ebase->d_zzcnce;
		if (ecotis->d_ztcnle<0) ecotis->d_ztcnle=0;
		ecotis->d_ztcule=ecotis->d_ztcuge-NbAnLis*ebase->d_zzcuge;
		if (ecotis->d_ztcule<0) ecotis->d_ztcule=0;
		ecotis->d_ztrglf=ecotis->d_ztrgif-NbAnLis*ebase->d_zzrgif;
		if (ecotis->d_ztrglf<0) ecotis->d_ztrglf=0;
		if (eindic->s_ztimci==1)
		{
			ecotis->d_ztchli=ecotis->d_ztchci-NbAnLis*ebase->d_zzchci;
			if (ecotis->d_ztchli<0) ecotis->d_ztchli=0;
		}
		else
		{
			ecotis->d_ztchli=0;
		}
		if (eindic->s_ztcoda==1)
		{	
			ecotis->d_ztchlm=ecotis->d_ztchcm-NbAnLis*ebase->d_zzchcm;
			if (ecotis->d_ztchlm<0) ecotis->d_ztchlm=0;
		}
		else
		{
			ecotis->d_ztchlm=0;
		}
		if (trace)
		{
			fprintf (efrescc,"   * calcul avec lissage *   \n");
			fprintf (efrescc,"		nombre année =%.0f\n",NbAnLis);
			fprintf (efrescc,"		assujetti tcci =%d\n",eindic->s_ztimci);
			fprintf (efrescc,"		assujetti DA =%d\n",eindic->s_ztcoda);
			fprintf (efrescc,"		D_ZTCNCT =%.0f\n",ecotis->d_ztcnct);
			fprintf (efrescc,"		D_ZZCNCT =%.0f\n",ebase->d_zzcnct);
			fprintf (efrescc,"		D_ZTCNLT =%.0f\n",ecotis->d_ztcnlt);
			fprintf (efrescc,"		D_ZTCNCS =%.0f\n",ecotis->d_ztcncs);
			fprintf (efrescc,"		D_ZZCNCS =%.0f\n",ebase->d_zzcncs);
			fprintf (efrescc,"		D_ZTCNLS =%.0f\n",ecotis->d_ztcnls);
			fprintf (efrescc,"		D_ZTCNCG =%.0f\n",ecotis->d_ztcncg);
			fprintf (efrescc,"		D_ZZCNCG =%.0f\n",ebase->d_zzcncg);
			fprintf (efrescc,"		D_ZTCNLG =%.0f\n",ecotis->d_ztcnlg);
			fprintf (efrescc,"		D_ZTCNCE =%.0f\n",ecotis->d_ztcnce);
			fprintf (efrescc,"		D_ZZCNCE =%.0f\n",ebase->d_zzcnce);
			fprintf (efrescc,"		D_ZTCNLE =%.0f\n",ecotis->d_ztcnle);
			fprintf (efrescc,"		D_ZTCUGE =%.0f\n",ecotis->d_ztcuge);
			fprintf (efrescc,"		D_ZZCUGE =%.0f\n",ebase->d_zzcuge);
			fprintf (efrescc,"		D_ZTCULE =%.0f\n",ecotis->d_ztcule);
			fprintf (efrescc,"		D_ZTRGIF =%.0f\n",ecotis->d_ztrgif);
			fprintf (efrescc,"		D_ZZRGIF =%.0f\n",ebase->d_zzrgif);
			fprintf (efrescc,"		D_ZTRGLF =%.0f\n",ecotis->d_ztrglf);
			fprintf (efrescc,"		D_ZTCHCI =%.0f\n",ecotis->d_ztchci);
			fprintf (efrescc,"		D_ZZCHCI =%.0f\n",ebase->d_zzchci);
			fprintf (efrescc,"		D_ZTCHLI =%.0f\n",ecotis->d_ztchli);
			fprintf (efrescc,"		D_ZTCHCM =%.0f\n",ecotis->d_ztchcm);
			fprintf (efrescc,"		D_ZZCHCM =%.0f\n",ebase->d_zzchcm);
			fprintf (efrescc,"		D_ZTCHLM =%.0f\n",ecotis->d_ztchlm);
		}
		ecotis->d_zttpav=ecotis->d_ztcnlt +			
							ecotis->d_ztcnls +
							ecotis->d_ztcnlg +
							ecotis->d_ztcnle +
							ecotis->d_ztcule +
							ecotis->d_ztrglf;
		if (trace)
		{
			fprintf (efrescc,"   * cotisation CFE lissee avant frais *   \n");
			fprintf (efrescc,"		D_ZTTPAV =%.0f\n",ecotis->d_zttpav);
		}


	if (trace)
	{
		fprintf (efrescc,"		* cotisation TCM avant frais \n");
		fprintf (efrescc,"		 d_ztchma=%.0f+%.0f \n",ecotis->d_ztchlm,drfixe);
	}
	ecotis->d_ztchma=ecotis->d_ztchlm + drfixe;
	if (trace)
	{
		fprintf (efrescc,"		 D_ZTCHMA=%.0f\n",ecotis->d_ztchma);
	}

	ecotis->d_zytpgp = ecotis->d_zttpgp;
	if (trace)
	{
		fprintf (efrescc,"		* cotisation TSE grand Paris non lissée \n");
		fprintf (efrescc,"		 d_zytpgp=%.0f \n",ecotis->d_zytpgp);
	}
	if (trace)
	{
		fprintf (efrescc,"		* cotisation TSE grand Paris lissée \n");
		fprintf (efrescc,"		 d_zttpgp=%.0f*%.0f/%.0f \n",ecotis->d_zttpgp,ecotis->d_ztcnle,ecotis->d_ztcnce);
	}
	if (!(ecotis->d_ztcnce == 0))
	{
		ecotis->d_zttpgp=arrondi(ecotis->d_zytpgp * ecotis->d_ztcnle / ecotis->d_ztcnce);
	}
	else
	{
		ecotis->d_zttpgp=0;
	}
	if (trace)
	{
		fprintf (efrescc,"		 D_ZTTPGP=%.0f\n",ecotis->d_zttpgp);
	}


	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	10. Lissage dormant\n");
		fprintf (efrescc,"   ***   \n");
	}


	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	11. Cotisations exonerees dormantes lissees \n");
		fprintf (efrescc,"   ***   \n");
	}
		if (trace)
		{
			fprintf (efrescc,"   * calcul avec lissage dormant *   \n");
			fprintf (efrescc,"		ZTTPXC =%.0f\n",zttpxc);
			fprintf (efrescc,"		ZTTPXS =%.0f\n",zttpxs);
			fprintf (efrescc,"		ZTTPXE =%.0f\n",zttpxe);
			fprintf (efrescc,"		ZTTPXT =%.0f\n",zttpxt);
			fprintf (efrescc,"		CEXMAPI =%.0f\n",cexmapi);
			fprintf (efrescc,"		CEXTASA =%.0f\n",cextasa);
			fprintf (efrescc,"		D_ZTTPXI =%.0f\n",ecotis->d_zttpxi);
			fprintf (efrescc,"		D_ZTTPXM =%.0f\n",ecotis->d_zttpxm);
		}
		zttpxc=zttpxc-NbAnLis*ebase->d_zxcnct;
		if (zttpxc<0) zttpxc=0;
		zttpxs=zttpxs-NbAnLis*ebase->d_zxcncs;
		if (zttpxs<0) zttpxs=0;
		zttpxe=zttpxe-NbAnLis*ebase->d_zxcncg;
		if (zttpxe<0) zttpxe=0;
		zttpxt=zttpxt-NbAnLis*ebase->d_zxcnce;
		if (zttpxt<0) zttpxt=0;
		cexmapi=cexmapi-NbAnLis*ebase->d_zxcuge;
		if (cexmapi<0) cexmapi=0;
		cextasa=cextasa-NbAnLis*ebase->d_zxrgif;
		if (cextasa<0) cextasa=0;
		if (eindic->s_ztimci==1)
		{
			ecotis->d_zttpli=ecotis->d_zttpxi-NbAnLis*ebase->d_zxchci;
			if (ecotis->d_zttpli<0) ecotis->d_zttpli=0;
		}
		else
		{
			ecotis->d_zttpli=0;
		}
		if (eindic->s_ztcoda==1)
		{	
			ecotis->d_zttplm=ecotis->d_zttpxm-NbAnLis*ebase->d_zxchcm;
			if (ecotis->d_zttplm<0) ecotis->d_zttplm=0;
		}
		else
		{
			ecotis->d_zttplm=0;
		}
		if (trace)
		{
			fprintf (efrescc,"		ZXCNCT =%.0f\n",ebase->d_zxcnct);
			fprintf (efrescc,"		ZTTPXC =%.0f\n",zttpxc);
			fprintf (efrescc,"		ZXCNCS =%.0f\n",ebase->d_zxcncs);
			fprintf (efrescc,"		ZTTPXS =%.0f\n",zttpxs);
			fprintf (efrescc,"		ZXCNCG =%.0f\n",ebase->d_zxcncg);
			fprintf (efrescc,"		ZTTPXE =%.0f\n",zttpxe);
			fprintf (efrescc,"		ZXCNCE =%.0f\n",ebase->d_zxcnce);
			fprintf (efrescc,"		ZTTPXT =%.0f\n",zttpxt);
			fprintf (efrescc,"		ZXCUGE =%.0f\n",ebase->d_zxcuge);
			fprintf (efrescc,"		CEXMAPI =%.0f\n",cexmapi);
			fprintf (efrescc,"		ZXRGIF =%.0f\n",ebase->d_zxrgif);
			fprintf (efrescc,"		CEXTASA =%.0f\n",cextasa);
			fprintf (efrescc,"		ZXCHCI =%.0f\n",ebase->d_zxchci);
			fprintf (efrescc,"		D_ZTTPLI =%.0f\n",ecotis->d_zttpli);
			fprintf (efrescc,"		ZXCHCM =%.0f\n",ebase->d_zxchcm);
			fprintf (efrescc,"		D_ZTTPLM =%.0f\n",ecotis->d_zttplm);
		}
		zttpx=zttpxc+zttpxs+zttpxt+cextasa+cexmapi+zttpxe;
		if (trace)
		{
			fprintf (efrescc,"   * cotisation exoneree lissee avant frais *   \n");
			fprintf (efrescc,"		ZTTPX =%.0f\n",zttpx);
		}


	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	12. Lissage dormant CVAE \n");
		fprintf (efrescc,"   ***   \n");
	}

	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	13. Cotisations exonerees dormantes CVAE lissees \n");
		fprintf (efrescc,"   ***   \n");
	}
		if (trace)
		{
			fprintf (efrescc,"   * calcul avec lissage dormant CVAE *   \n");
			fprintf (efrescc,"		ZZTPXC =%.0f\n",zztpxc);
			fprintf (efrescc,"		ZZTPXS =%.0f\n",zztpxs);
			fprintf (efrescc,"		ZZTPXE =%.0f\n",zztpxe);
			fprintf (efrescc,"		ZZTPXT =%.0f\n",zztpxt);
			fprintf (efrescc,"		CEXMAPICVAE =%.0f\n",cexmapicvae);
			fprintf (efrescc,"		CEXTASACVAE =%.0f\n",cextasacvae);
		}
		zztpxc=zztpxc-NbAnLis*ebase->d_zxcnzt;
		if (zztpxc<0) zztpxc=0;
		zztpxs=zztpxs-NbAnLis*ebase->d_zxcnzs;
		if (zztpxs<0) zztpxs=0;
		zztpxe=zztpxe-NbAnLis*ebase->d_zxcnzg;
		if (zztpxe<0) zztpxe=0;
		zztpxt=zztpxt-NbAnLis*ebase->d_zxcnze;
		if (zztpxt<0) zztpxt=0;
		cexmapicvae=cexmapicvae-NbAnLis*ebase->d_zxcuze;
		if (cexmapicvae<0) cexmapicvae=0;
		cextasacvae=cextasacvae-NbAnLis*ebase->d_zxrgzf;
		if (cextasacvae<0) cextasacvae=0;
		if (trace)
		{
			fprintf (efrescc,"		ZXCNZT =%.0f\n",ebase->d_zxcnzt);
			fprintf (efrescc,"		ZZTPXC =%.0f\n",zztpxc);
			fprintf (efrescc,"		ZXCNZS =%.0f\n",ebase->d_zxcnzs);
			fprintf (efrescc,"		ZZTPXS =%.0f\n",zztpxs);
			fprintf (efrescc,"		ZXCNZG =%.0f\n",ebase->d_zxcnzg);
			fprintf (efrescc,"		ZZTPXE =%.0f\n",zztpxe);
			fprintf (efrescc,"		ZXCNZE =%.0f\n",ebase->d_zxcnze);
			fprintf (efrescc,"		ZZTPXT =%.0f\n",zztpxt);
			fprintf (efrescc,"		ZXCUZE =%.0f\n",ebase->d_zxcuze);
			fprintf (efrescc,"		CEXMAPICVAE =%.0f\n",cexmapicvae);
			fprintf (efrescc,"		ZXRGZF =%.0f\n",ebase->d_zxrgzf);
			fprintf (efrescc,"		CEXTASACVAE =%.0f\n",cextasacvae);
		}
		zztpx=zztpxc+zztpxs+zztpxt+cextasacvae+cexmapicvae+zztpxe;
		if (trace)
		{
			fprintf (efrescc,"   * cotisation exoneree CVAE lissee avant frais *   \n");
			fprintf (efrescc,"		ZZTPX =%.0f\n",zztpx);
		}



	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	9. FRAIS DE GESTION CFE \n");
		fprintf (efrescc,"   ***   \n");
	}
	zttpxf = 0;
	zztpxf = 0;
	fraisbmin = 0;
		F1_cne=arrondi(ecotis->d_ztcnlt*0.01);
		F1_epci=arrondi(ecotis->d_ztcnlg*0.01);
		ecotis->d_zttpf1=(F1_cne+F1_epci);
		F2_cne=arrondi(ecotis->d_ztcnlt*0.02);
		F2_epci=arrondi(ecotis->d_ztcnlg*0.02);
		ecotis->d_zttpf2=(F2_cne+F2_epci);
		ecotis->d_ztcum1=arrondi(ecotis->d_ztcule*0.01);
		ecotis->d_ztcum2=arrondi(ecotis->d_ztcule*0.02);
		ecotis->d_zttpf0=arrondi(ecotis->d_ztrglf*0.044);
		ecotis->d_zttpf7=arrondi(ecotis->d_ztrglf*0.036);
		ecotis->d_zttpf4=arrondi(ecotis->d_ztcnls*0.044);
		ecotis->d_zttpf3=arrondi(ecotis->d_ztcnls*0.036);
		ecotis->d_zttpf5=arrondi(ecotis->d_ztcnle*0.054);
		ecotis->d_zttpf6=arrondi(ecotis->d_ztcnle*0.036);
		ecotis->d_zttpfs=((ecotis->d_zttpf1+ecotis->d_zttpf2+ecotis->d_zttpf3+ecotis->d_zttpf4+ecotis->d_zttpf5+ecotis->d_zttpf6+ecotis->d_ztcum1+ecotis->d_ztcum2+ecotis->d_zttpf0+ecotis->d_zttpf7));			
		if (trace)
		{
			fprintf (efrescc,"		*FAR CFE 1%% \n");
			fprintf (efrescc,"		 D_ZTTPF1=arrondi((%.0f+%.0f) x 0.01))\n",ecotis->d_ztcnlt,ecotis->d_ztcnlg);
			fprintf (efrescc,"		 D_ZTTPF1=%.0f\n",floor(ecotis->d_zttpf1));
			fprintf (efrescc,"		*FDNV CFE 2%% \n");
			fprintf (efrescc,"		 D_ZTTPF2=arrondi((%.0f+%.0f) x 0.02))\n",ecotis->d_ztcnlt,ecotis->d_ztcnlg);
			fprintf (efrescc,"		 D_ZTTPF2=%.0f\n",floor(ecotis->d_zttpf2));
			fprintf (efrescc,"		*FAR TGMAPI 1%% \n");
			fprintf (efrescc,"		 D_ZTCUM1=arrondi(%.0f) x 0.01)\n",ecotis->d_ztcule);
			fprintf (efrescc,"		 D_ZTCUM1=%.0f\n",floor(ecotis->d_ztcum1));
			fprintf (efrescc,"		*FDNV TGMAPI 2%% \n");
			fprintf (efrescc,"		 D_ZTCUM2=arrondi(%.0f) x 0.02)\n",ecotis->d_ztcule);
			fprintf (efrescc,"		 D_ZTCUM2=%.0f\n",floor(ecotis->d_ztcum2));
			fprintf (efrescc,"		*FAR TASARIF 4,4%% \n");
			fprintf (efrescc,"		 D_ZTTPF0=arrondi(%.0f) x 0.044)\n",ecotis->d_ztrglf);
			fprintf (efrescc,"		 D_ZTTPF0=%.0f\n",floor(ecotis->d_zttpf0));
			fprintf (efrescc,"		*FDNV TASARIF 3,6%% \n");
			fprintf (efrescc,"		 D_ZTTPF7=arrondi(%.0f) x 0.036))\n",ecotis->d_ztrglf);
			fprintf (efrescc,"		 D_ZTTPF7=%.0f\n",floor(ecotis->d_zttpf7));
			fprintf (efrescc,"		*FAR CFE 4,4%% \n");
			fprintf (efrescc,"		 D_ZTTPF4=arrondi(%.0f) x 0.044)\n",ecotis->d_ztcnls);
			fprintf (efrescc,"		 D_ZTTPF4=%.0f\n",floor(ecotis->d_zttpf4));
			fprintf (efrescc,"		*FDNV CFE 3,6%% \n");
			fprintf (efrescc,"		 D_ZTTPF3=arrondi(%.0f) x 0.036))\n",ecotis->d_ztcnls);
			fprintf (efrescc,"		 D_ZTTPF3=%.0f\n",floor(ecotis->d_zttpf3));
			fprintf (efrescc,"		*FAR CFE 5,4%% \n");
			fprintf (efrescc,"		 D_ZTTPF5=arrondi(%.0f) x 0.054))\n",ecotis->d_ztcnle);
			fprintf (efrescc,"		 D_ZTTPF5=%.0f\n",floor(ecotis->d_zttpf5));
			fprintf (efrescc,"		*FDNV CFE 3,6%% \n");
			fprintf (efrescc,"		 D_ZTTPF6=arrondi(%.0f) x 0.036))\n",ecotis->d_ztcnle);
			fprintf (efrescc,"		 D_ZTTPF6=%.0f\n",floor(ecotis->d_zttpf6));
			fprintf (efrescc,"		***  TOTAL FRAIS DE GESTION CFE  \n");
			fprintf (efrescc,"		 D_ZTTPFS=(%.0f+%.0f+%.0f)\n",floor(ecotis->d_zttpf1),floor(ecotis->d_zttpf2),floor(ecotis->d_zttpf3));
			fprintf (efrescc,"		+%.0f+%.0f+%.0f\n",floor(ecotis->d_zttpf4),floor(ecotis->d_zttpf5),floor(ecotis->d_zttpf6));
			fprintf (efrescc,"		+%.0f+%.0f\n",floor(ecotis->d_ztcum1),floor(ecotis->d_ztcum2));
			fprintf (efrescc,"		+%.0f+%.0f\n",floor(ecotis->d_zttpf0),floor(ecotis->d_zttpf7));
			fprintf (efrescc,"		 D_ZTTPFS=%.0f\n",ecotis->d_zttpfs);
		}

		zoneaC = 0;
		zoneaE = 0;
		zonebC = 0;
		zonebE = 0;
		zonec = 0;
		zoned = 0;
		zonee = 0;
		zonef = 0;
		zoneg = 0;
		zoneh = 0;

		zoneaC=arrondi(zttpxc*0.01);
		zoneaE=arrondi(zttpxe*0.01);
		zonebC=arrondi(zttpxc*0.02);
		zonebE=arrondi(zttpxe*0.02);
		zonec=(arrondi(cextasa*0.044)+arrondi(zttpxs*0.044));
		zoned=(arrondi(cextasa*0.036)+arrondi(zttpxs*0.036));
		zonee=arrondi(zttpxt*0.054);
		zonef=arrondi(zttpxt*0.036);
		zoneg=arrondi(cexmapi*0.01);
		zoneh=arrondi(cexmapi*0.02);			
		zttpxf=zoneaC+zoneaE+zonebC+zonebE+zonec+zoned+zonee+zonef+zoneg+zoneh;			
		if (trace)
		{
			fprintf (efrescc,"	****FRAIS EXONERES  ZTTPXF ** \n");
			fprintf (efrescc," zttpxf=(%.0f +%.0f +%.0f +%.0f+%.0f+%.0f+%.0f+%.0f+%.0f+%.0f)\n",zoneaC,zoneaE,zonebC,zonebE,zonec,zoned,zonee,zonef,zoneg,zoneh);
			fprintf (efrescc,"		 zttpxf=%.0f\n",zttpxf);
		}

		zoneavC = 0;
		zoneavE = 0;
		zonebvC = 0;
		zonebvE = 0;
		zonec = 0;
		zoned = 0;
		zonee = 0;
		zonef = 0;
		zoneg = 0;
		zoneh = 0;

		zoneavC=arrondi(zztpxc*0.01);
		zoneavE=arrondi(zztpxe*0.01);
		zonebvC=arrondi(zztpxc*0.02);			
		zonebvE=arrondi(zztpxe*0.02);			
		zonec=(arrondi(cextasacvae*0.044)+arrondi(zztpxs*0.044));
		zoned=(arrondi(cextasacvae*0.036)+arrondi(zztpxs*0.036));
		zonee=arrondi(zztpxt*0.054);
		zonef=arrondi(zztpxt*0.036);
		zoneg=arrondi(cexmapicvae*0.01);
		zoneh=arrondi(cexmapicvae*0.02);			
		zztpxf=zoneavC+zoneavE+zonebvC+zonebvE+zonec+zoned+zonee+zonef+zoneg+zoneh;			
		if (trace)
		{
			fprintf (efrescc,"	******FRAIS EXONERES CVAE   ZZTPXF\n");
			fprintf (efrescc," zztpxf=(%.0f +%.0f +%.0f+%.0f+%.0f+%.0f+%.0f+%.0f+%.0f+%.0f)\n",zoneavC,zoneavE,zonebvC,zonebvE,zonec,zoned,zonee,zonef,zoneg,zoneh);
			fprintf (efrescc,"		 zztpxf=%.0f\n",zztpxf);
		}
	
		zonea = 0;
		zoneb = 0;
		zonec = 0;
		zoned = 0;
		zonee = 0;
		zonef = 0;
		zoneg = 0;
		zoneh = 0;

		zonea=(arrondi(cbmincne*0.01)+arrondi(cbminepci*0.01));	
		zoneb=(arrondi(cbmincne*0.02)+arrondi(cbminepci*0.02));	
		zonec=(arrondi(cbmintasa*0.044)+arrondi(cbminsy*0.044));	
		zoned=(arrondi(cbmintasa*0.036)+arrondi(cbminsy*0.036));	
		zonee=arrondi(cbmintse*0.054);	
		zonef=arrondi(cbmintse*0.036);
		zoneg=arrondi(cbminmapi*0.01);
		zoneh=arrondi(cbminmapi*0.02);						
		fraisbmin=(zonea+zoneb+zonec+zoned+zonee+zonef+zoneg+zoneh);
		if (trace)
		{
			fprintf (efrescc,"	****FRAIS SUR BASE MINIMUM-1 \n");
			fprintf (efrescc," fraisbmin=(%.0f +%.0f+%.0f+%.0f+%.0f+%.0f+%.0f+%.0f)\n",zonea,zoneb,zonec,zoned,zonee,zonef,zoneg,zoneh);
			fprintf (efrescc,"		 fraisbmin=%.0f\n",fraisbmin);
		}

    Ce dégrèvement peut être remis à zéro si la cotisation mise en recouvrement est inférieure à 12 */
		if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	DEGREVEMENT COVID19\n");
		fprintf (efrescc,"   ***   \n");
		if (DegrevementNaf)
		{
			fprintf (efrescc,"DEGREVEMENT\n");
		}
		else
		{
			fprintf (efrescc,"PAS DEGREVEMENT\n");
		}
		fprintf (efrescc,"		c_cnac3 =%s\n",eindic->c_cnac3);
		fprintf (efrescc,"		D_ZTET47 =%.0f\n",eindic->d_ztet47);
		fprintf (efrescc,"		LIMITE CA =%.0f\n",LimiteCA);
		fprintf (efrescc,"		*indicateur de prise en compte degrèvement (s_zttpd7) \n");
		fprintf (efrescc,"		 s_ZTTPD7=%d\n",eindic->s_zttpd7);
		fprintf (efrescc,"		*délibération commune (d_ztcnco) \n");
		fprintf (efrescc,"		 d_ztcnco=%.0f\n",etaux->d_ztcnco);
		fprintf (efrescc,"		*délibération epci (d_ztcuco) \n");
		fprintf (efrescc,"		 d_ztcuco=%.0f\n",etaux->d_ztcuco);
	}

	if (eindic->s_zttpd7 == 1 && DegrevementNaf)
	{
		if (etaux->d_ztcnco == 10000)
		{
			ecotis->d_ztcn19=arrondi((ecotis->d_ztcnlt+F1_cne+F2_cne)*2/3);
			ecotis->d_ztcnc9=arrondi(ecotis->d_ztcnlt/3);
			DegExoC=arrondi((zttpxc+zoneaC+zonebC)*2/3);
			DegExocvaeC=arrondi((zztpxc+zoneavC+zonebvC)*2/3);
		}
		else
		{
			ecotis->d_ztcn19=0;
			ecotis->d_ztcnc9=0;
			DegExoC = 0;
			DegExocvaeC = 0;
		}
		if (etaux->d_ztcuco == 10000)
		{
			ecotis->d_ztcu19=arrondi((ecotis->d_ztcnlg+F1_epci+F2_epci)*2/3);
			ecotis->d_ztcuc9=arrondi(ecotis->d_ztcnlg/3);
			DegExoE=arrondi((zttpxe+zoneaE+zonebE)*2/3);
			DegExocvaeE=arrondi((zztpxe+zoneavE+zonebvE)*2/3);
		}
		else
		{
			ecotis->d_ztcu19=0;
			ecotis->d_ztcuc9=0;
			DegExoE = 0;
			DegExocvaeE = 0;
		}
		ecotis->d_zttpc9=(ecotis->d_ztcn19 - ecotis->d_ztcnc9) + (ecotis->d_ztcu19 - ecotis->d_ztcuc9);
	}
	else
	{
		ecotis->d_ztcn19=0;
		ecotis->d_ztcu19=0;
		ecotis->d_ztcnc9=0;
		ecotis->d_ztcuc9=0;
		ecotis->d_zttpc9=0;
		DegExoC = 0;
		DegExoE = 0;
		DegExocvaeC = 0;
		DegExocvaeE = 0;
	}
		if (trace)
	{
		fprintf (efrescc,"		*DEGREVEMENT COMMUNE\n");
		fprintf (efrescc,"		 d_ztcn19=%.0f+%.0f+%.0f*2/3\n",ecotis->d_ztcnlt,F1_cne,F2_cne);
		fprintf (efrescc,"		 D_ZTCN19=%.0f\n",ecotis->d_ztcn19);
		fprintf (efrescc,"		*DEGREVEMENT EPCI\n");
		fprintf (efrescc,"		 d_ztcu19=%.0f+%.0f+%.0f*2/3\n",ecotis->d_ztcnlg,F1_epci,F2_epci);
		fprintf (efrescc,"		 D_ZTCU19=%.0f\n",ecotis->d_ztcu19);
		fprintf (efrescc,"		*PART COMMUNE\n");
		fprintf (efrescc,"		 d_ztcnc9=%.0f*1/3\n",ecotis->d_ztcnlt);
		fprintf (efrescc,"		 D_ZTCNC9=%.0f\n",ecotis->d_ztcnc9);
		fprintf (efrescc,"		*PART EPCI\n");
		fprintf (efrescc,"		 d_ztcuc9=%.0f*1/3\n",ecotis->d_ztcnlg);
		fprintf (efrescc,"		 D_ZTCUC9=%.0f\n",ecotis->d_ztcuc9);
		fprintf (efrescc,"		*PART ETAT\n");
		fprintf (efrescc,"		 d_zttpc9=%.0f-%.0f+%.0f-%.0f\n",ecotis->d_ztcn19,ecotis->d_ztcnc9,ecotis->d_ztcu19,ecotis->d_ztcuc9);
		fprintf (efrescc,"		 D_ZTTPC9=%.0f\n",ecotis->d_zttpc9);
	}



	ecotis->d_zttpap=ecotis->d_zttpav + ecotis->d_zttpfs - ecotis->d_ztcn19 - ecotis->d_ztcu19;
	ecotis->d_zttpxl=zttpx+zttpxf-DegExoC-DegExoE;
	ecotis->d_zztpxl=zztpx+zztpxf-DegExocvaeC-DegExocvaeE;
	ecotis->d_zttpbm=cbminmapi+cbmincne+cbminepci+cbminsy+cbmintse+fraisbmin+cbmintasa;
	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	10. COTISATION CFE APRES FAIS DE GESTION(D_ZTTPAP)\n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"		*COTISATION CFE apres frais=somme de la cotisation CFE lissee avant frais et des frais de gestion CFE\n");
		fprintf (efrescc,"		 d_zttpap=%.0f+%.0f-%.0f-%.0f\n",ecotis->d_zttpav,ecotis->d_zttpfs,ecotis->d_ztcn19,ecotis->d_ztcu19);
		fprintf (efrescc,"		 D_ZTTPAP=%.0f\n",ecotis->d_zttpap);
		fprintf (efrescc,"		* COTISATION EXONEREE apres frais (D_ZTTPXL)=somme des cotisations exonerees  plus frais exoneres\n");
		fprintf (efrescc,"		 d_zttpxl=%.0f+%.0f-%.0f-%.0f\n",zttpx,zttpxf,DegExoC,DegExoE);
		fprintf (efrescc,"		 D_ZTTPXL=%.0f\n",ecotis->d_zttpxl);
		fprintf (efrescc,"		*COTISATION EXONEREE CVAE apres frais (D_ZZTPXL)=somme des cotisations exonerees cvae  plus frais exoneres cvae\n");
		fprintf (efrescc,"		 d_zztpxl=%.0f+%.0f-%.0f-%.0f\n",zztpx,zztpxf,DegExocvaeC,DegExocvaeE);
		fprintf (efrescc,"		 D_ZZTPXL=%.0f\n",ecotis->d_zztpxl);
		fprintf (efrescc,"		*COTISATION BASE MINIMUMD_ZTTPBM)=somme des cotisations sur ebase minimum plus frais ebase minimum\n");
		fprintf (efrescc,"		 d_zttpbm=%.0f+%.0f+%.0f+%.0f+%.0f+%.0f+%.0f\n",cbminmapi,cbmincne,cbminepci,cbminsy,cbmintse,fraisbmin,cbmintasa);
		fprintf (efrescc,"		 D_ZTTPBM=%.0f\n",ecotis->d_zttpbm);
	}

	farexo = 0;
	fdnvexo = 0;
	ecotis->d_ztchfa=arrondi(ecotis->d_ztchli*0.054);
	ecotis->d_ztchfd=arrondi(ecotis->d_ztchli*0.036);
	farexo=arrondi(ecotis->d_zttpli*0.054);
	fdnvexo=arrondi(ecotis->d_zttpli*0.036);
	if (trace)
	{
		fprintf (efrescc,"	11. FRAIS DE GESTION TCCI  \n");
		fprintf (efrescc,"		*FAR TCCI 5,4%% \n");
		fprintf (efrescc,"		 D_ZTCHFA=arrondi(%.0f x 0.054)\n",ecotis->d_ztchli);
		fprintf (efrescc,"		 D_ZTCHFA=%.0f\n",floor(ecotis->d_ztchfa));
		fprintf (efrescc,"		*FDNV TCCI 3,6%% \n");
		fprintf (efrescc,"		 D_ZTCHFD=arrondi(%.0f x 0.036)\n",ecotis->d_ztchli);
		fprintf (efrescc,"		 D_ZTCHFD=%.0f\n",floor(ecotis->d_ztchfd));
		fprintf (efrescc,"	******FRAIS DE GESTION TCCI EXONEREE \n");
		fprintf (efrescc,"		*FAR TCCI EXO 5,4%% \n");
		fprintf (efrescc,"		 farexo=arrondi(%.0f x 0.054)\n",ecotis->d_zttpli);
		fprintf (efrescc,"		 farexo=%.0f\n",floor(farexo));
		fprintf (efrescc,"		*FDNV TCCI EXO 3,6%% \n");
		fprintf (efrescc,"		 fdnvexo=arrondi(%.0f x 0.036)\n",ecotis->d_zttpli);
		fprintf (efrescc,"		 fdnvexo=%.0f\n",floor(fdnvexo));
	}


	ecotis->d_ztchcc=ecotis->d_ztchli + ecotis->d_ztchfa + ecotis->d_ztchfd;
	ecotis->d_zttpli=ecotis->d_zttpli + farexo + fdnvexo;
	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	12. cotisation brute TCCI apres frais de gestion avant degrevement\n");
		fprintf (efrescc,"     (D_ZTCHCC)\n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"		*cotisation TCCI apres frais de gestion\n");
		fprintf (efrescc,"		 d_ztchcc=%.0f+%.0f+%.0f\n",ecotis->d_ztchli,ecotis->d_ztchfa,ecotis->d_ztchfd);
		fprintf (efrescc,"		 D_ZTCHCC=%.0f\n",ecotis->d_ztchcc);
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	12. cotisation brute TCCI exoneree apres frais de gestion avant degrevement\n");
		fprintf (efrescc,"     (D_ZTTPLI)\n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"		*cotisation TCCI EXONEREE apres frais de gestion\n");
		fprintf (efrescc,"		 D_ZTTPLI=%.0f\n",ecotis->d_zttpli);
	}

	tcm = ecotis->d_ztchlm + drfixe;
	farexo = 0;
	fdnvexo = 0;
	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	13. FRAIS DE GESTION TCM \n");
		fprintf (efrescc,"   ***   \n");
	}
	
	if (alsace_moselle)
	{
		ecotis->d_zzchma=arrondi(tcm*0.03);
		ecotis->d_zzchmd=arrondi(tcm*0.05); 		
		farexo=arrondi(ecotis->d_zttplm*0.03);
		fdnvexo=arrondi(ecotis->d_zttplm*0.05); 		
		if (trace)
		{
			fprintf (efrescc,"		a) Departements d'Alsace-Moselle (570,670 et 680)\n");	
			fprintf (efrescc,"		*FAR TCMA 3%% \n");
			fprintf (efrescc,"		 D_ZZCHMA=arrondi(%.0f x 0.03)\n",tcm);
			fprintf (efrescc,"		 D_ZZCHMA=%.0f\n",floor(ecotis->d_zzchma));
			fprintf (efrescc,"		*FDNV TCMA 5%% \n");
			fprintf (efrescc,"		 D_ZZCHMD=arrondi(%.0f x 0.05)\n",tcm);
			fprintf (efrescc,"		 D_ZZCHMD=%.0f\n",floor(ecotis->d_zzchmd));
			fprintf (efrescc,"		*FAR TCMA EXO 3%% \n");
			fprintf (efrescc,"		 farexo=arrondi(%.0f x 0.03)\n",ecotis->d_zttplm);
			fprintf (efrescc,"		 farexo=%.0f\n",floor(farexo));
			fprintf (efrescc,"		*FDNV TCMA EXO 5%% \n");
			fprintf (efrescc,"		 fdnvexo=arrondi(%.0f x 0.05)\n",ecotis->d_zttplm);
			fprintf (efrescc,"		 fdnvexo=%.0f\n",floor(fdnvexo));
		}
	}
	else
	{
       	ecotis->d_zzchma=arrondi(tcm*0.054);
		ecotis->d_zzchmd=arrondi(tcm*0.036); 		
	    farexo=arrondi(ecotis->d_zttplm*0.054);
		fdnvexo=arrondi(ecotis->d_zttplm*0.036); 		
		if (trace)
		{
			fprintf (efrescc,"		b) Autres departements\n");		
			fprintf (efrescc,"		*FAR TCMA 5,4%% \n");
			fprintf (efrescc,"		 D_ZZCHMA=arrondi(%.0f x 0.054)\n",tcm);
			fprintf (efrescc,"		 D_ZZCHMA=%.0f\n",floor(ecotis->d_zzchma));
			fprintf (efrescc,"		*FDNV TCMA 3,6%% \n");
			fprintf (efrescc,"		 D_ZZCHMD=arrondi(%.0f x 0.036)\n",tcm);
			fprintf (efrescc,"		 D_ZZCHMD=%.0f\n",floor(ecotis->d_zzchmd));
			fprintf (efrescc,"		*FAR TCMA EXO 3%% \n");
			fprintf (efrescc,"		 farexo=arrondi(%.0f x 0.054)\n",ecotis->d_zttplm);
			fprintf (efrescc,"		 farexo=%.0f\n",floor(farexo));
			fprintf (efrescc,"		*FDNV TCMA EXO 5%% \n");
			fprintf (efrescc,"		 fdnvexo=arrondi(%.0f x 0.036)\n",ecotis->d_zttplm);
			fprintf (efrescc,"		 fdnvexo=%.0f\n",floor(fdnvexo));
		}
	}

	ecotis->d_ztchm=tcm + ecotis->d_zzchma + ecotis->d_zzchmd;
	ecotis->d_zttplm=ecotis->d_zttplm + farexo + fdnvexo;
	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	14. cotisation TCM apres frais de gestion (D_ZTCHM)\n");
		fprintf (efrescc,"		*cotisation TCM apres frais=somme de la cotisation TCM lissee avant frais et des \n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"     		frais de gestion TCM\n");
		fprintf (efrescc,"		 d_ztchm=%.0f+%.0f+%.0f \n",tcm,ecotis->d_zzchma,ecotis->d_zzchmd);
		fprintf (efrescc,"		 D_ZTCHM=%.0f\n",ecotis->d_ztchm);
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	14. cotisation TCM exoneree apres frais de gestion (D_ZTTPLM)\n");
		fprintf (efrescc,"		*cotisation TCM EXONEREE apres frais=somme de la cotisation TCM lissee avant frais et des \n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"     		frais de gestion EXONERE TCM lissee \n");
		fprintf (efrescc,"		 D_ZTTPLM=%.0f\n",ecotis->d_zttplm);
	}


	ecotis->d_zttpct=ecotis->d_zttpap +
				ecotis->d_ztcn19 + ecotis->d_ztcu19 +
				ecotis->d_ztchcc +
				ecotis->d_ztchm +
				ecotis->d_zttpeo +
				ecotis->d_zttphy +			
				ecotis->d_zttpe +			
				ecotis->d_zttpch +
				ecotis->d_zttpph +				
				ecotis->d_zttpt1 +
				ecotis->d_zttpt2 +
				ecotis->d_zttpt3 +			
				ebase->d_zttpsi +			
				ecotis->d_zxtpg1 +
				ecotis->d_zxtpg2 +
				ecotis->d_zxtpg3 +
				ecotis->d_zxtpg4 +
				ecotis->d_zxtpg5 +
				ecotis->d_zxtpg6 +			
				ecotis->d_zxtpg8 +			
				ecotis->d_zttpoe +			
				ecotis->d_zttpo1 +			
				ecotis->d_zttpo2 +			
				ecotis->d_zttpmr +	
                ecotis->d_zzsnr1 +
				ecotis->d_zzsnr2 +
				ecotis->d_zzsnr3 +
				ecotis->d_zzsnr4 +			
				ecotis->d_zxtpr2 +
				ecotis->d_zxtpr4 +
				ecotis->d_zxtpr6 +
				ecotis->d_zttpe1 +
				ecotis->d_zttpe2 +
	            ecotis->d_zttpn1 +
				ecotis->d_zttpn2 +
            	ecotis->d_zttph1 +
				ecotis->d_zttph2 +
	            ecotis->d_zztpt1 +
				ecotis->d_zztpt2 +
			   	ecotis->d_zttps1 +
				ecotis->d_zttps2 +
			   	ecotis->d_zttpg1 +
				ecotis->d_zttpg2 +
             	ecotis->d_zttpq1 +
 				ecotis->d_zttpq2 +
             	ecotis->d_ztsnf1 +
				ecotis->d_ztsnf2 +
	          	ecotis->d_zytpr1 +
     			ecotis->d_zytpr2;
	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	15. cotisation CFE,TCCI et TCM mise en recouvrement (D_ZTTPCT)\n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	d_zttpct=%.0f+%.0f+%.0f+%.0f+%.0f+%.0f+%.0f+%.0f  \n",ecotis->d_zttpap,ecotis->d_ztcn19,ecotis->d_ztcu19,ecotis->d_ztchcc,ecotis->d_ztchm,ecotis->d_zttpeo,ecotis->d_zttphy,ecotis->d_zttpe);
		fprintf (efrescc,"  +%.0f+%.0f+%.0f+%.0f+%.0f+%.0f  \n",ecotis->d_zttpch,ecotis->d_zttpph,ecotis->d_zttpt1,ecotis->d_zttpt2,ecotis->d_zttpt3,ebase->d_zttpsi);
		fprintf (efrescc,"  +%.0f+%.0f+%.0f  \n",ecotis->d_zxtpg1,ecotis->d_zxtpg2,ecotis->d_zxtpg3);
		fprintf (efrescc,"  +%.0f+%.0f+%.0f+%.0f  \n",ecotis->d_zxtpg4,ecotis->d_zxtpg5,ecotis->d_zxtpg6,ecotis->d_zxtpg8);
		fprintf (efrescc,"  +%.0f+%.0f+%.0f+%.0f  \n",ecotis->d_zttpoe,ecotis->d_zttpo1,ecotis->d_zttpo2,ecotis->d_zttpmr);
		fprintf (efrescc,"  +%.0f+%.0f+%.0f+%.0f+%.0f+%.0f+%.0f  \n",ecotis->d_zzsnr1,ecotis->d_zzsnr2,ecotis->d_zzsnr3,ecotis->d_zzsnr4,ecotis->d_zxtpr2,ecotis->d_zxtpr4,ecotis->d_zxtpr6);
		fprintf (efrescc,"  +%.0f+%.0f+%.0f+%.0f+%.0f+%.0f  \n",ecotis->d_zttpe1,ecotis->d_zttpe2,ecotis->d_zttpn1,ecotis->d_zttpn2,ecotis->d_zttph1,ecotis->d_zttph2);
		fprintf (efrescc,"  +%.0f+%.0f+%.0f  \n",ecotis->d_zztpt1,ecotis->d_zztpt2,ecotis->d_zttps1);
		fprintf (efrescc,"  +%.0f+%.0f+%.0f+%.0f+%.0f+%.0f  \n",ecotis->d_zttps2,ecotis->d_zttpg1,ecotis->d_zttpg2,ecotis->d_zttpq1,ecotis->d_zttpq2,ecotis->d_ztsnf1);
		fprintf (efrescc,"  +%.0f+%.0f+%.0f   \n",ecotis->d_ztsnf2,ecotis->d_zytpr1,ecotis->d_zytpr2);
		fprintf (efrescc,"		 D_ZTTPCT=%.0f\n",ecotis->d_zttpct);
	}

	if (ecotis->d_zttpct < 12)
	{
		ecotis->d_zttpap = ecotis->d_zttpap + ecotis->d_ztcn19 + ecotis->d_ztcu19;
		ecotis->d_zttpxl = ecotis->d_zttpxl + DegExoC + DegExoE;
		ecotis->d_zztpxl = ecotis->d_zztpxl + DegExocvaeC + DegExocvaeE;
		ecotis->d_ztcn19 = 0;
		ecotis->d_ztcu19 = 0;
		ecotis->d_ztcnc9 = 0;
		ecotis->d_ztcuc9 = 0;
		ecotis->d_zttpc9 = 0;
		DegExoC = 0;
		DegExoE = 0;
		DegExocvaeC = 0;
		DegExocvaeE = 0;
	}
	ecotis->d_ztexmi=0;
	zonea = 0;
	zoneb = 0;
	zonec = 0;
	zoned = 0; 				
	if (ebase->d_zzcnex == ebase->d_zxcnmi)					
	{
		ecotis->d_ztexmi=ecotis->d_ztexmi + zttpxc + zttpxs;
		zonea=arrondi(zttpxc*0.01);
		zoneb=arrondi(zttpxc*0.02);
		zonec=arrondi(zttpxs*0.044);
		zoned=arrondi(zttpxs*0.036);
		if (trace)
		{
			fprintf (efrescc,"   ***   \n");
			fprintf (efrescc,"	16. COTISATION EXONEREES MINIMIS ET RGEC\n");
			fprintf (efrescc,"   ***   \n");
			fprintf (efrescc,"	       a) Minimis\n");	
			fprintf (efrescc,"   *** commune,syndicat  \n");		
			fprintf (efrescc," D_ZTEXMI=(%.0f+%.0f+%.0f+%.0f+%.0f-%.0f)\n",ecotis->d_ztexmi,zonea,zoneb,zonec,zoned,DegExoC);
		}
		ecotis->d_ztexmi=ecotis->d_ztexmi + zonea + zoneb + zonec + zoned - DegExoC;
		if (trace)
		{
			fprintf (efrescc," D_ZTEXMI=%.0f\n",ecotis->d_ztexmi);
		}
	}

	zonea = 0;
	zoneb = 0;
	if (ebase->d_zzcuex == ebase->d_zxcumi)					
	{
		ecotis->d_ztexmi=ecotis->d_ztexmi + zttpxe;
	    zonea=arrondi(zttpxe*0.01);
		zoneb=arrondi(zttpxe*0.02);
		if (trace)
		{
			fprintf (efrescc,"   *** EPCI  \n");		
			fprintf (efrescc," D_ZTEXMI=(%.0f+%.0f+%.0f-%.0f)\n",ecotis->d_ztexmi,zonea,zoneb,DegExoE);
		}
	    ecotis->d_ztexmi=ecotis->d_ztexmi + zonea + zoneb + DegExoE;
		if (trace)
		{
			fprintf (efrescc," D_ZTEXMI=%.0f\n",ecotis->d_ztexmi);
		}
	}

	zonea = 0;
	zoneb = 0;
	if ((strncmp(etaux->c_cicuu,"U",sizeof(etaux->c_cicuu)) == 0) || (strncmp(eindic->c_cietz, "Z",sizeof(eindic->c_cietz)) == 0) || (strncmp(eindic->c_cietz, "O",sizeof(eindic->c_cietz)) == 0))
	{
		if (eindic->s_ztcnep == 1)
		{
			if (ebase->d_zzcuex == ebase->d_zxcumi)					
			{
				zonea=arrondi(zttpxt*0.054);
				zoneb=arrondi(zttpxt*0.036);
				if (trace)
				{
					fprintf (efrescc,"   *** FPU TSE  \n");		
					fprintf (efrescc," D_ZTEXMI=(%.0f+%.0f+%.0f+%.0f)\n",ecotis->d_ztexmi,zttpxt,zonea,zoneb);
				}
				ecotis->d_ztexmi=ecotis->d_ztexmi + zttpxt;
				ecotis->d_ztexmi=ecotis->d_ztexmi + zonea + zoneb;
				if (trace) fprintf (efrescc," D_ZTEXMI=%.0f\n",ecotis->d_ztexmi);
				zonea=arrondi(cextasa*0.044);
				zoneb=arrondi(cextasa*0.036);
				if (trace)
				{
					fprintf (efrescc,"   *** FPU TASARIF  \n");		
					fprintf (efrescc," D_ZTEXMI=(%.0f+%.0f+%.0f+%.0f)\n",ecotis->d_ztexmi,cextasa,zonea,zoneb);
				}
				ecotis->d_ztexmi=ecotis->d_ztexmi + cextasa;
				ecotis->d_ztexmi=ecotis->d_ztexmi + zonea + zoneb;
				if (trace) fprintf (efrescc," D_ZTEXMI=%.0f\n",ecotis->d_ztexmi);
			}
		}
		zonea = 0;
		zoneb = 0;
		if (trace)
		{
			fprintf (efrescc,"  ** INDICATEUR Collectivite TGEMAPI **\n");					
			fprintf (efrescc,"      C_CITPAI =%s\n",eindic->c_citpai);
		}
	    if ((strncmp(eindic->c_citpai, "E",sizeof(eindic->c_citpai)) == 0) || (strncmp(eindic->c_citpai, "C",sizeof(eindic->c_citpai))  == 0))
		{
			if (ebase->d_zzcuex == ebase->d_zxcumi) 					
			{
				zonea=arrondi(cexmapi*0.01);
				zoneb=arrondi(cexmapi*0.02);
				if (trace)
				{
					fprintf (efrescc,"   *** FPU TGEMAPI  \n");		
					fprintf (efrescc," D_ZTEXMI=(%.0f+%.0f+%.0f+%.0f)\n",ecotis->d_ztexmi,cexmapi,zonea,zoneb);
				}
				ecotis->d_ztexmi=ecotis->d_ztexmi + cexmapi;
				ecotis->d_ztexmi=ecotis->d_ztexmi + zonea + zoneb;
				if (trace) fprintf (efrescc," D_ZTEXMI=%.0f\n",ecotis->d_ztexmi);
			}
		}
	}
	else
	{
		if (eindic->s_ztcnep == 1)
		{
			if (ebase->d_zzcnex == ebase->d_zxcnmi)					
			{
				zonea=arrondi(zttpxt*0.054);
				zoneb=arrondi(zttpxt*0.036);
				if (trace)
				{
					fprintf (efrescc,"   *** NON FPU TSE  \n");		
					fprintf (efrescc," D_ZTEXMI=(%.0f+%.0f+%.0f+%.0f)\n",ecotis->d_ztexmi,zttpxt,zonea,zoneb);
				}
				ecotis->d_ztexmi=ecotis->d_ztexmi + zttpxt;
				ecotis->d_ztexmi=ecotis->d_ztexmi + zonea + zoneb;
				if (trace) fprintf (efrescc," D_ZTEXMI=%.0f\n",ecotis->d_ztexmi);
				zonea=arrondi(cextasa*0.044);
				zoneb=arrondi(cextasa*0.036);
				if (trace)
				{
					fprintf (efrescc,"   ***NON  FPU TASARIF  \n");		
					fprintf (efrescc," D_ZTEXMI=(%.0f+%.0f+%.0f+%.0f)\n",ecotis->d_ztexmi,cextasa,zonea,zoneb);
				}
				ecotis->d_ztexmi=ecotis->d_ztexmi + cextasa;
				ecotis->d_ztexmi=ecotis->d_ztexmi + zonea + zoneb;
				if (trace) fprintf (efrescc," D_ZTEXMI=%.0f\n",ecotis->d_ztexmi);
			}
		}
		zonea = 0;
		zoneb = 0;
		if (trace)
		{
			fprintf (efrescc,"  ** INDICATEUR Collectivite TGEMAPI **\n");					
			fprintf (efrescc,"      C_CITPAI =%s\n",eindic->c_citpai);
		}
		if ((strncmp(eindic->c_citpai, "E",sizeof(eindic->c_citpai)) == 0) || (strncmp(eindic->c_citpai, "C",sizeof(eindic->c_citpai)) == 0))
		{
			if (ebase->d_zzcnex == ebase->d_zxcnmi)					
			{
				zonea=arrondi(cexmapi*0.01);
				zoneb=arrondi(cexmapi*0.02);
				if (trace)
				{
					fprintf (efrescc,"   ***NON  FPU TGEMAPI  \n");		
					fprintf (efrescc," D_ZTEXMI=(%.0f+%.0f+%.0f+%.0f)\n",ecotis->d_ztexmi,cexmapi,zonea,zoneb);
				}
				ecotis->d_ztexmi=ecotis->d_ztexmi + cexmapi;
				ecotis->d_ztexmi=ecotis->d_ztexmi + zonea + zoneb;
				if (trace) fprintf (efrescc," D_ZTEXMI=%.0f\n",ecotis->d_ztexmi);
			}
		}
	}
	zonea = 0;
	zoneb = 0;
	if (ebase->d_zzchxi == ebase->d_zxchmi)
	{
		zonec=CalcUneecotis(ebase->d_zzchxi,etaux->d_ztcnci);
		ecotis->d_ztexmi = ecotis->d_ztexmi + zonec;
		zonea=arrondi(zonec*0.054);
		zoneb=arrondi(zonec*0.036);
		if (trace)
		{
			fprintf (efrescc,"   ***TCCI\n");
			fprintf (efrescc," ZONEC=%.0f\n",zonec);
			fprintf (efrescc," D_ZTEXMI=(%.0f+%.0f+%.0f)\n",ecotis->d_ztexmi,zonea,zoneb);
		}
		ecotis->d_ztexmi=ecotis->d_ztexmi + zonea + zoneb;
		if (trace) fprintf (efrescc," D_ZTEXMI=%.0f\n",ecotis->d_ztexmi);
	}
	zonea = 0;
	zoneb = 0;
	if (ebase->d_zzchxm == ebase->d_zychmi)
	{
		zonec=CalcUneecotis(ebase->d_zzchxm,etaux->d_ztch4);
		ecotis->d_ztexmi = ecotis->d_ztexmi + zonec;
		if (trace)
		{
			fprintf (efrescc,"   ***TCMA\n");
			fprintf (efrescc," ZONEC=(%.0fx%.0f=%.0f)\n",ebase->d_zzchxm,etaux->d_ztch4,zonec);
			fprintf (efrescc," D_ZTEXMI=(%.0f+%.0f+%.0f)\n",ecotis->d_ztexmi,zonea,zoneb);
			fprintf (efrescc," D_ZTEXMI=%.0f\n",ecotis->d_ztexmi);
		}
		if (alsace_moselle)
		{
			zonea=arrondi(zonec*0.03);
			zoneb=arrondi(zonec*0.05);
		}
		else
		{
			zonea=arrondi(zonec*0.054);
			zoneb=arrondi(zonec*0.036);
		}
		ecotis->d_ztexmi=ecotis->d_ztexmi + zonea + zoneb;
		if (trace) fprintf (efrescc," D_ZTEXMI=%.0f\n",ecotis->d_ztexmi);
	}
	ecotis->d_ztexrc=0;
	zonea = 0;
	zoneb = 0;
	zonec = 0;
	zoned = 0;					
	if (ebase->d_zzcnex == ebase->d_zxcnaf)					
	{
		zonea=arrondi(zttpxc*0.01);
		zoneb=arrondi(zttpxc*0.02);
		zonec=arrondi(zttpxs*0.044);
		zoned=arrondi(zttpxs*0.036);
		if (trace)
		{
			fprintf (efrescc,"   ***   \n");
			fprintf (efrescc,"	       b) REGLEMENT AFR (RGEC)\n");	
			fprintf (efrescc,"   *** commune,syndicat  \n");		
			fprintf (efrescc," D_ZTEXRC=(%.0f+%.0f+%.0f+%.0f+%.0f+%.0f+%.0f-%.0f)\n",ecotis->d_ztexrc,zttpxc,zttpxs,zonea,zoneb,zonec,zoned,DegExoC);
			ecotis->d_ztexrc=ecotis->d_ztexrc + zttpxc + zttpxs;
			ecotis->d_ztexrc=ecotis->d_ztexrc + zonea + zoneb + zonec + zoned + DegExoC;
			if (trace) fprintf (efrescc," D_ZTEXRC=%.0f\n",ecotis->d_ztexrc);
		}
	}
	zonea = 0;
	zoneb = 0;
	if (ebase->d_zzcuex == ebase->d_zxcuaf)					
	{
		zonea=arrondi(zttpxe*0.01);
		zoneb=arrondi(zttpxe*0.02);
		if (trace)
		{
			fprintf (efrescc,"   *** EPCI  \n");		
			fprintf (efrescc," D_ZTEXRC=(%.0f+%.0f+%.0f+%.0f-%.0f)\n",ecotis->d_ztexrc,zttpxe,zonea,zoneb,DegExoE);
		}
		ecotis->d_ztexrc=ecotis->d_ztexrc + zttpxe;
		ecotis->d_ztexrc=ecotis->d_ztexrc + zonea + zoneb + DegExoE;
		if (trace) fprintf (efrescc," D_ZTEXRC=%.0f\n",ecotis->d_ztexrc);
	}
	zonea = 0;
	zoneb = 0;
	if ((strncmp(etaux->c_cicuu,"U",sizeof(etaux->c_cicuu)) == 0) || (strncmp(eindic->c_cietz, "Z",sizeof(eindic->c_cietz)) == 0) || (strncmp(eindic->c_cietz, "O",sizeof(eindic->c_cietz)) == 0))
	{
		if (eindic->s_ztcnep == 1)
		{
			if (ebase->d_zzcuex == ebase->d_zxcuaf)					
			{
				zonea=arrondi(zttpxt*0.054);
				zoneb=arrondi(zttpxt*0.036);
				if (trace)
				{
					fprintf (efrescc,"   *** FPU TSE  \n");		
					fprintf (efrescc," D_ZTEXRC=(%.0f+%.0f+%.0f+%.0f)\n",ecotis->d_ztexrc,zttpxt,zonea,zoneb);
				}
				ecotis->d_ztexrc=ecotis->d_ztexrc + zttpxt;
				ecotis->d_ztexrc=ecotis->d_ztexrc + zonea + zoneb;
				if (trace) fprintf (efrescc," D_ZTEXRC=%.0f\n",ecotis->d_ztexrc);
				zonea=arrondi(cextasa*0.044);
				zoneb=arrondi(cextasa*0.036);
				if (trace)
				{
					fprintf (efrescc,"   *** FPU TASARIF  \n");		
					fprintf (efrescc," D_ZTEXRC=(%.0f+%.0f+%.0f+%.0f)\n",ecotis->d_ztexrc,cextasa,zonea,zoneb);
				}
				ecotis->d_ztexrc=ecotis->d_ztexrc + cextasa;
				ecotis->d_ztexrc=ecotis->d_ztexrc + zonea + zoneb;
				if (trace) fprintf (efrescc," D_ZTEXRC=%.0f\n",ecotis->d_ztexrc);
			}
		}
		zonea = 0;
		zoneb = 0;
		if (trace)
		{
			fprintf (efrescc,"  ** INDICATEUR Collectivite TGEMAPI **\n");					
			fprintf (efrescc,"      C_CITPAI =%s\n",eindic->c_citpai);
		}
		if ((strncmp(eindic->c_citpai, "E",sizeof(eindic->c_citpai)) == 0) || (strncmp(eindic->c_citpai, "C",sizeof(eindic->c_citpai)) == 0))
		{
			if (ebase->d_zzcuex == ebase->d_zxcuaf)					
			{
				zonea=arrondi(cexmapi*0.01);
				zoneb=arrondi(cexmapi*0.02);
				if (trace)
				{
					fprintf (efrescc,"   *** FPU TGEMAPI  \n");		
					fprintf (efrescc," D_ZTEXRC=(%.0f+%.0f+%.0f+%.0f)\n",ecotis->d_ztexrc,cexmapi,zonea,zoneb);
				}
				ecotis->d_ztexrc=ecotis->d_ztexrc + cexmapi;
				ecotis->d_ztexrc=ecotis->d_ztexrc + zonea + zoneb;
				if (trace) fprintf (efrescc," D_ZTEXRC=%.0f\n",ecotis->d_ztexrc);
			}
		}
	}
	else
	{
		zonea = 0;
		zoneb = 0;
		if (eindic->s_ztcnep == 1)
		{
			if (ebase->d_zzcnex == ebase->d_zxcnaf)					
			{
				zonea=arrondi(zttpxt*0.054);
				zoneb=arrondi(zttpxt*0.036);
				if (trace)
				{
					fprintf (efrescc,"   *** NON FPU TSE  \n");		
					fprintf (efrescc," D_ZTEXRC=(%.0f+%.0f+%.0f+%.0f)\n",ecotis->d_ztexrc,zttpxt,zonea,zoneb);
				}
				ecotis->d_ztexrc=ecotis->d_ztexrc + zttpxt;
				ecotis->d_ztexrc=ecotis->d_ztexrc + zonea + zoneb;
				if (trace) fprintf (efrescc," D_ZTEXRC=%.0f\n",ecotis->d_ztexrc);
				zonea=arrondi(cextasa*0.044);
				zoneb=arrondi(cextasa*0.036);
				if (trace)
				{
					fprintf (efrescc,"   ***NON  FPU TASARIF  \n");		
					fprintf (efrescc," D_ZTEXRC=(%.0f+%.0f+%.0f+%.0f)\n",ecotis->d_ztexrc,cextasa,zonea,zoneb);
				}
				ecotis->d_ztexrc=ecotis->d_ztexrc + cextasa;
				ecotis->d_ztexrc=ecotis->d_ztexrc + zonea + zoneb;
				if (trace) fprintf (efrescc," D_ZTEXRC=%.0f\n",ecotis->d_ztexrc);
			}
		}
		zonea = 0;
		zoneb = 0;
		if (trace)
		{
			fprintf (efrescc,"  ** INDICATEUR Collectivite TGEMAPI **\n");					
			fprintf (efrescc,"      C_CITPAI =%s\n",eindic->c_citpai);
		}
		if ((strncmp(eindic->c_citpai, "E",sizeof(eindic->c_citpai)) == 0) || (strncmp(eindic->c_citpai, "C",sizeof(eindic->c_citpai)) == 0))
		{
			if (ebase->d_zzcnex == ebase->d_zxcnaf)					
			{
				zonea=arrondi(cexmapi*0.01);
				zoneb=arrondi(cexmapi*0.02);
				if (trace)
				{
					fprintf (efrescc,"   ***NON  FPU TGEMAPI  \n");		
					fprintf (efrescc," D_ZTEXRC=(%.0f+%.0f+%.0f+%.0f)\n",ecotis->d_ztexrc,cexmapi,zonea,zoneb);
				}
				ecotis->d_ztexrc=ecotis->d_ztexrc + cexmapi;
				ecotis->d_ztexrc=ecotis->d_ztexrc + zonea + zoneb;
				if (trace) fprintf (efrescc," D_ZTEXRC=%.0f\n",ecotis->d_ztexrc);
			}
		}
	}
	zonea = 0;
	zoneb = 0;
	if (ebase->d_zzchxi == ebase->d_zxchaf)
	{
		zonec=CalcUneecotis(ebase->d_zzchxi,etaux->d_ztcnci);
		ecotis->d_ztexrc = ecotis->d_ztexrc + zonec;
		zonea=arrondi(zonec*0.054);
		zoneb=arrondi(zonec*0.036);
		if (trace)
		{
			fprintf (efrescc,"   ***TCCI\n");
			fprintf (efrescc," ZONEC=%.0f\n",zonec);
			fprintf (efrescc," D_ZTEXRC=(%.0f+%.0f+%.0f)\n",ecotis->d_ztexrc,zonea,zoneb);
		}
		ecotis->d_ztexrc=ecotis->d_ztexrc + zonea + zoneb;
		if (trace) fprintf (efrescc," D_ZTEXRC=%.0f\n",ecotis->d_ztexrc);
	}
	zonea = 0;
	zoneb = 0;
	if (ebase->d_zzchxm == ebase->d_zychaf)
	{
		zonec=CalcUneecotis(ebase->d_zzchxm,etaux->d_ztch4);
		ecotis->d_ztexrc = ecotis->d_ztexrc + zonec;
		if (trace)
		{
			fprintf (efrescc,"   ***TCMA\n");
			fprintf (efrescc," ZONEC=%.0f\n",zonec);
			fprintf (efrescc," D_ZTEXRC=(%.0f+%.0f+%.0f)\n",ecotis->d_ztexrc,zonea,zoneb);
			fprintf (efrescc," D_ZTEXRC=%.0f\n",ecotis->d_ztexrc);
		}
		if (alsace_moselle)
		{
			zonea=arrondi(zonec*0.03);
			zoneb=arrondi(zonec*0.05);
		}
		else
		{
			zonea=arrondi(zonec*0.054);
			zoneb=arrondi(zonec*0.036);
		}
		ecotis->d_ztexrc=ecotis->d_ztexrc + zonea + zoneb;
		if (trace) fprintf (efrescc," D_ZTEXRC=%.0f\n",ecotis->d_ztexrc);
	}

	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	17. Credit d'impots zone defense(D_ZTTPCD)\n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"		*indicateur de prise en compte credit impot (s_zttpd6) \n");
		fprintf (efrescc,"		 s_ZTTPD6=%d\n",eindic->s_zttpd6);
		fprintf (efrescc,"		*indicateur commune zone defense (c_cicnzd) \n");
		fprintf (efrescc,"		 c_cicnzd=%s\n",etaux->c_cicnzd);
	}
	if ((eindic->s_zttpd6 == 1) && (strncmp(etaux->c_cicnzd,"O",sizeof(etaux->c_cicnzd)) == 0))
	{
		ecotis->d_zttpcd=(eindic->d_ztetzd*75);	
		if (ecotis->d_zttpcd > 8175)
		{
			ecotis->d_zttpcd = 8175;
		}
		ecotis->d_ztexmi=ecotis->d_ztexmi+ecotis->d_zttpcd;
		if (trace)
		{
			fprintf (efrescc,"		******************************************************** \n");
			fprintf (efrescc,"	 	*Credit d'impot=produit du nombre de salaries en zone defense(D_ZTETZD)\n");
			fprintf (efrescc,"     		par 75(750 euros/10 car le nombre de salaries est decimal)\n");
			fprintf (efrescc,"     	plafonne  à 8175 (10,9 salaries * 750) soit:\n");
			fprintf (efrescc,"		 D_ZTTPCD=minimum[(D_ZTETZDx75),8175]\n");
			fprintf (efrescc,"		 d_zttpcd=minimum[(%.0fx75),8175]\n",eindic->d_ztetzd);			
			fprintf (efrescc,"		 D_ZTTPCD=%.0f\n",ecotis->d_zttpcd);
			fprintf (efrescc,"		 D_ZTEXMI=D_ZTEXMI+D_ZTTPCD    minimis\n");
			fprintf (efrescc,"		 d_ztexmi=%.0f,%.0f\n",ecotis->d_ztexmi,ecotis->d_zttpcd);			
			fprintf (efrescc,"		 D_ZTEXMI=%.0f\n",ecotis->d_ztexmi);
		}
	}


	ecotis->d_zttpcp = ecotis->d_zttpct - ecotis->d_ztcn19 - ecotis->d_ztcu19;
	cnp=ecotis->d_zttpcp;
	if (cnp >= ecotis->d_zttpcd)
	{
		ecotis->d_zttpcp = cnp -  ecotis->d_zttpcd;
	    ecotis->d_zttprd = 0;
	}
	if (cnp < ecotis->d_zttpcd)
	{
		ecotis->d_zttpcp = 0;
		ecotis->d_zttprd =  ecotis->d_zttpcd - cnp;
	}
	if (trace)
	{
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"	18. Somme a payer\n");
		fprintf (efrescc,"   ***   \n");
		fprintf (efrescc,"		*Somme a payer avant credit\n");
		fprintf (efrescc,"		 cnp=%.0f\n",cnp);
		fprintf (efrescc,"		*credit impot\n");
		fprintf (efrescc,"		 D_ZTTPCD=%.0f\n",ecotis->d_zttpcd);
		fprintf (efrescc,"		*Credit restituable(D_ZTTPRD)\n");
		fprintf (efrescc,"		 D_ZTTPRD=%.0f\n",ecotis->d_zttprd);
		fprintf (efrescc,"		    * \n");
		fprintf (efrescc,"		*Somme a payer apres credit\n");
		fprintf (efrescc,"		 D_ZTTPCP=%.0f\n",ecotis->d_zttpcp);
		fprintf (efrescc,"		    * \n");
	}

}
if (trace)
{
	fprintf (efrescc," [- CFECALC2021.C fin -]\n");
	fprintf (efrescc," *******************************\n");
}
return 0;
}




